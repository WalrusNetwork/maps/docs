var sections = [];
var nav_wrapper;
var content_wrapper;
var count;
var guide;

$(document).ready(function() {
    nav_wrapper = $('#guide-nav');
    content_wrapper = $('#guide-tabs');
    count = $('h3').length;

    parseQuiz();
    
    // setup sections
    $('h3').each(function(i) {
        var title = $(this).text();
        var anchor = $(this).attr('id');
        var section = $(this).nextUntil('h3'); // use .addBack() to include header
        // append pill to the section nav
        $('<li/>', {'class': 'nav-item'}).append([
            $('<a/>', {
                'class': 'nav-link guide-link',
                'id': anchor + '-tab',
                'data-toggle': 'pill',
                'data-index': i,
                'href': '#' + anchor,
                'text': i+1 + '. ' + title
            })
        ]).appendTo(nav_wrapper);
        // append section content to tabs
        $('<div/>', {
            'class': 'tab-pane fade',
            'id': anchor,
            'aria-labelledby': anchor + '-tab'
        }).appendTo(content_wrapper);
        $(content_wrapper).children().last().append(section).html(); // needs to be done after to preserve html
        sections.push('#' + anchor);
    });
    $('#content #raw').empty();
    
    // create new guide record
    guide = {
        path: current_path,
        title: $('#page-head').text(),
        current: {
            anchor: sections[0],
            index: 0
        },
        complete: false
    }
    
    // save guide record or fetch existing record and update tabs
    if (user.addGuide(guide)) {
        user.save();
    } else {
        guide.current = user.getGuideCurrent(guide.path);
        // check that saved index is not out of scope
        if (guide.current.index >= count) {
            guide.current.index = 0;
            guide.complete = false;
        }
    }
    changePanel(guide.current.index);
    loadGuideStatus();
    
    // handle previous button click
    $('#prev').click(function() {
        if (guide.current.index > 0) {
            var to = guide.current.index - 1;
            changePanel(to);
        }
    });
    
    // handle next button click
    $('#next').click(function() {
        if (guide.current.index < count) {
            var to = guide.current.index + 1;
            changePanel(to);
        }
    });
    
    // handle click on tab pill
    $('#guide-nav').on('click', 'li a', function() {
        var to = $(this).data('index');
        changePanel(to);
    });
    
    // handle quiz response
    $('#content').on('click', '.quiz-responses:not(.answered) .quiz-response', function() {
        var responded = $(this).data('response');
        var responses = $(this).closest('.quiz-responses');
        var reason = $(this).closest('.guide-quiz').find('.quiz-reason').show();
        var correct = responses.data('correct');
        responses.addClass('answered');
        responses.find("[data-response='" + correct + "']").addClass('correct');
        reason.show();
        if (responded != correct) {
            reason.prepend("<span class='text-danger font-weight-bold'>Incorrect.</span> ");
            responses.find("[data-response='" + responded + "']").addClass('incorrect');
        } else {
            reason.prepend("<span class='text-success font-weight-bold'>Correct!</span> ");
            confetti.start();
            setTimeout(confetti.stop, 2000);
        }
    });
});

function parseQuiz() {
    var raw = $('#raw').html();
    var quizzes = raw.match(/\[quiz .+?\]/g);
    if (quizzes) {
        for (var i = 0; i < quizzes.length; i++) {
            // extract quiz data
            var questionRegex = /(?:question=”)(.+?)(?=”)/g;
            var responsesRegex = /(?:responses=”)(.+?)(?=”)/g;
            var correctRegex = /(?:correct=”)(.+?)(?=”)/g;
            var reasonRegex = /(?:reason=”)(.+?)(?=”)/g;
            var question = questionRegex.exec(quizzes[i])[1];
            var responses = responsesRegex.exec(quizzes[i])[1].split(';');
            var correct = correctRegex.exec(quizzes[i])[1];
            var reason = reasonRegex.exec(quizzes[i])[1];
            // generate quiz html
            var responsesHtml = $('<div/>', {
                'class': 'quiz-responses',
                'data-correct': correct
            });
            for (var j = 0; j < responses.length; j++) {
                var responseHtml = $("<div/>", {
                    'class': 'quiz-response',
                    'data-response': j,
                    'text': responses[j]
                })
                responsesHtml.append(responseHtml);
            }
            var quizHtml = $('<div/>', {'class': 'guide-quiz'}).append([
                $('<div/>', {
                    'class': 'quiz-question',
                    'text': question
                }),
                $('<div/>', {
                    'class': 'quiz-reason',
                    'css': {
                        'display': 'none'
                    },
                    'text': reason
                })
            ]).append(responsesHtml);
            var wrapper = $('<div/>').append(quizHtml);
            // replace shortcode with generated html
            raw = raw.replace(toRegexExpression(quizzes[i]), wrapper.html());
        }
        $('#raw').html(raw);
    }
}

function toRegexExpression(string) {
    string = string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    var regex = new RegExp(string, 'g');
    return regex;
}

// changes the active panel to the supplied index
function changePanel(to) {
    var current = {
        index: to,
        anchor: sections[to]
    }
    if (user.updateGuideCurrent(current_path, current)) {
        user.save();
        guide.current = current;
        
        // update pills and panels
        var nav_wrapper = $('#guide-nav');
        var content_wrapper = $('#guide-tabs');
        nav_wrapper.find('li > a').removeClass('active');
        nav_wrapper.find('li > a' + current.anchor + '-tab').addClass('active');
        content_wrapper.children().removeClass('show active');
        content_wrapper.find(current.anchor).addClass('show active');

        // update navigation buttons
        var prevBtn = $('#prev');
        var nextBtn = $('#next');
        var facetsBtn = $('#facets');
        if (Number(current.index) === Number(0)) {
            prevBtn.hide();
            nextBtn.show().find('span').text('Start');
            facetsBtn.hide();
        } else if (Number(current.index) === Number(count - 1)) {
            prevBtn.show();
            nextBtn.hide();
            facetsBtn.show();
            if (user.completeGuide(current_path)) {
                user.save();
                user.loadSave();
                loadGuideStatus();
            }
        } else {
            prevBtn.show();
            nextBtn.show().find('span').text('Next');
            facetsBtn.hide();
        }
    }
    $('html, body').animate({ scrollTop: 0 }, 'fast');
}

// marks guides that have been fully read
function loadGuideStatus() {
    var status = user._guidesStatus;
    var guides = $('.nav-item.toc-item.guide');
    guides.each(function() {
       for (var i = 0; i < status.length; i++) {
           if ($(this).find('span').text().trim() === status[i].title.trim() && status[i].complete) {
               $(this).addClass('complete');
               $(this).find('i').removeClass('fa-circle').addClass('fa-check-circle');
           }
       }
    });
}
