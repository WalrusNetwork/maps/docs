$(document).ready(function() {
    if (current_path != '/') {
        // display bookmark icons
        $('#content:not(.guide) > h1, #content:not(.guide) > h3, #content:not(.guide) > h4, #content:not(.guide) > h5').each(function() {
            $(this).prepend('<i class="bookmark far fa-bookmark"></i>');
        });

        // handle bookmark requests
        $('.bookmark').click(function() {
            var bookmarked = $(this).hasClass('bookmarked');
            var parent = $(this).parent();
            var bookmark = {
                path: current_path,
                anchor: parent.attr('id')
            }
            // is this bookmark already checked?
            if (bookmarked) {
                // remove the bookmark
                var index = user.getBookmarkIndex(bookmark);
                user.removeBookmark(index);
            } else {
                // create new bookmark record
                var current_section = parent.prop('nodeName');
                var current_node = parent;
                var next_section = {
                    'H6': 'h5',
                    'H5': 'h4',
                    'H4': 'h3',
                    'H3': false
                }
                var breadcrumbs = "";
                // are we bookmaring the page header?
                if (current_section != 'H1') {
                    // insert section info into bookmark
                    bookmark["info"] = parent.next().text().substring(0, 150) + '...';
                    // get each of the section titles to create breadcumbs
                    do {
                        breadcrumbs = current_node.text().trim() + breadcrumbs;
                        if (current_section != undefined) {
                            breadcrumbs = ' / ' + breadcrumbs;
                            current_node = parent.prevUntil(next_section[current_section]).last().prev();
                            current_section = current_node.prop('nodeName');
                        }
                    } while (current_section != undefined);
                } else {
                    // if bookmarking page header, get first paragraph content instead
                    bookmark["info"] = $('main').find('p:first').text().substring(0, 150) + '...';
                }
                // append page header
                var page_header = $('h1:first').text().trim();
                bookmark["breadcrumbs"] = page_header + breadcrumbs;
                // add the new bookmark
                user.addBookmark(bookmark);
            }
            // save the user
            user.save();
            // toggle the icon class
            $(this).toggleClass('far fas bookmarked');
        });

        // highlight bookmarked sections on load
        showBookmarkedSections();
    }

    // bookmarks book
    $('#userBookmarksModal').on('show.bs.modal', function (e) {
        showBookmarksList();
        handleBookmarksRemoveButtons();
        $('#user-reset-bookmarks').click(resetUserBookmarks);
    });
});

function showBookmarkedSections() {
    for (var i = 0; i < user._bookmarks.length; i++) {
        if (user._bookmarks[i].path == current_path) {
            $('.bookmark').parent().each(function() {
                if ($(this).attr('id') == user._bookmarks[i].anchor) {
                    $(this).find('.bookmark').toggleClass('far fas bookmarked');
                }
            });
        }
    }
}

function handleBookmarksRemoveButtons() {
    $('.bookmark-remove').click(function() {
        var target = $(this).data('target');
        if (user.removeBookmark(target)) {
            user.save();
            showBookmarksList();
            handleBookmarksRemoveButtons();
            showBookmarkedSections();
        }
    });
}

function showBookmarksList() {
    var b = user._bookmarks;
    if (b.length) {
        $('.no-bookmarks').hide();
        $('.bookmarks-wrapper').text('');
        for (var i = 0; i < b.length; i++) {
            $('<div/>', {'class': 'bookmark-item'}).append([
                $('<i/>', {
                    'class': 'fas fa-times bookmark-remove',
                    'data-target': i
                }),
                $('<a/>', {
                    'href': b[i].path + '#' + b[i].anchor,
                    'text': b[i].breadcrumbs
                }),
                $('<span/>', {
                    'class': 'text-mute small d-block',
                    'text': b[i].info
                })
            ]).appendTo('.bookmarks-wrapper');
        }
    } else {
        $('.no-bookmarks').show();
        $('.bookmarks-wrapper').text('');
    }
}

function resetUserBookmarks() {
    if (confirm('Are you sure you want to reset your bookmarks?')) {
        if (user.resetBookmarks()) {
            user.save();
            $('.no-bookmarks').show();
            $('.bookmarks-wrapper').text('');
            $('.bookmarked').removeClass('bookmarked');
            showBookmarkedSections();
        }
    }
}
