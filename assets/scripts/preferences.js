---
---

var user;

$(document).ready(function() {
    user = new User();
    applyUserPreferences();
    $('#userPreferencesModal').on('show.bs.modal', function (e) {
        loadUserPreferenceOptions();
        $('#user-save-preferences').click(saveUserPreferences);
        $('#user-reset-preferences').click(resetUserPreferences);
    });
});

function applyUserPreferences() {
    if (user._preferences.theme == 'dark') {
        $('head').append('<link id="theme-dark" rel="stylesheet" href="{{ site.baseurl }}/assets/css/dark.css"/>');
        var sheet = document.getElementById("theme-dark");
        sheet.onload = function() {
            $('#theme-default').remove();
        }
    }
    if (user._preferences.dyslexia) {
        $('head').append('<link id="font-dyslexia" rel="stylesheet" href="https://cdn.clarkhacks.com/OpenDyslexic/v3/OpenDyslexic.css" type="text/css">');
    }
}

function loadUserPreferenceOptions() {
    var preferences = user._preferences;
    $('.preference').each(function() {
        var type = $(this).attr('name');
        var val = preferences[type];
        if (typeof val !== 'undefined') {
            if (typeof val == 'boolean') {
                val = val.toString();
            }
            $(this).val(val);
        }
    });
}

function saveUserPreferences() {
    var preferences = {};
    $('.preference').each(function() {
        var type = $(this).attr('name');
        var val = $(this).find('option:selected').val();
        // convert string boolean to boolean type
        if (val == 'true' || val == 'false') {
            val = (val == 'true');
        }
        preferences[type] = val;
    });
    if (user.setPreferences(preferences)) {
        sendAlert('Successfully updated your site settings!', 'alert-success');
        saveUser();
        if (user._preferences.theme == 'dark') {
            if ($('#theme-dark').length == 0) {
                $('head').append('<link id="theme-dark" rel="stylesheet" href="{{ site.baseurl }}/assets/css/dark.css"/>');
                var sheet = document.getElementById("theme-dark");
                sheet.onload = function() {
                    $('#theme-default').remove();
                }
            }
        } else {
            if ($('#theme-default').length == 0) {
                $('head').append('<link id="theme-default" rel="stylesheet" href="{{ site.baseurl }}/assets/css/default.css"/>');
                var sheet = document.getElementById("theme-default");
                sheet.onload = function() {
                    $('#theme-dark').remove();
                }
            }
        }
        if (user._preferences.dyslexia == true) {
            if ($('#font-dyslexia').length == 0) {
                $('head').append('<link id="font-dyslexia" rel="stylesheet" href="https://cdn.clarkhacks.com/OpenDyslexic/v3/OpenDyslexic.css" type="text/css">');
            }
        } else {
            $('#font-dyslexia').remove();
        }
    } else {
        sendAlert('Unable to update your site settings. Please try again.', 'alert-danger');
    }
}

function resetUserPreferences() {
    if (confirm('Are you sure you want to reset your settings?')) {
        if (user.resetPreferences()) {
            sendAlert('Your site settings have been reset.', 'alert-success');
            saveUser();
            loadUserPreferenceOptions();
        } else {
            sendAlert('Unable to reset your site settings. Please try again.', 'alert-danger');
        }
    }
}

function saveUser() {
    if (user.save()) {
        // do nothing
    } else {
        sendAlert('Something went wrong when saving your User. Please view the console for more details.', 'alert-warning');
    }
}

function sendAlert(message, style = 'alert-info') {
    $('#user-alert').text(message);
    $('#user-alert').attr('class', 'alert ' + style);
    $('#user-alert-wrapper').show().delay(5000).fadeOut();
}
