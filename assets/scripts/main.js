var current_path = window.location.pathname;

$(document).ready(function() {
    // set the scroll position of the sidebar to the active group
    if ($('sidebar .toc').length) {
        var toc_active_group = $('sidebar .toc .toc-group.active').position();
        $('sidebar .toc .toc-nav').scrollTop(toc_active_group.top - 70);
    }

    // collapsed sidebar icon toggler
    $('#sidebar-toggler').on('click', function () {
        $(this)
            .find('i')
            .toggleClass('fa-chevron-down')
            .toggleClass('fa-chevron-right');
    });

    // bootstrap tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // copy code blocks
    $('pre.highlight').hover(
        function() {
            var clipboard = new ClipboardJS('.btn-copy', {
                container: document.getElementsByTagName('pre.highlight').children,
                target: function(trigger) {
                    return trigger.nextElementSibling;
                }
            }).on('success', function() {
                $('.btn-copy').text('Copied!');
            });
            $(this).children(':first').before('<button type="button" class="btn btn-sm btn-outline-secondary btn-copy">Copy</button>');
        }, function() {
            $(this).find('button:last').remove();
        }
    );
});
