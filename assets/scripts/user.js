class User {
    constructor() {
        this._preferences = {
            theme: "default",
            dyslexia: false
        };
        this._bookmarks = [];
        this._guidesStatus = [];
        // check if pre-existing User data exists
        if (this.saveExists()) {
            this.loadSave();
        }
    }

    /** 
     * Takes an Object of preference ID value key pairs and sets
     * them as the current User preferences.
     *
     * @param  {object}  Parsed preferences to apply to the user
     * @return {boolean} Success or fail response
     */
    setPreferences(p) {
        const properties = ["theme", "dyslexia"]
        try {
            for (var key in p) {
                if (properties.includes(key)) {
                    this._preferences[key] = p[key];
                } else {
                    console.warn('Unknown User preference property:', key)
                }
            }
            return true;
        } catch (e) {
            return false;
        }
    }

    /** 
     * Resets the User preferences to the defaults.
     *
     * @return {boolean} Success or fail response
     */
    resetPreferences() {
        this._preferences = {
            theme: "default",
            dyslexia: false
        };
        return true;
    }

    /** 
     * Gets the index value of the passed bookmark
     * object. Returns false if no bookmarks match.
     *
     * @param  {object}  Bookmark object
     * @return {integer} Bookmark index in array
     */
    getBookmarkIndex(b) {
        for (var i = 0; i < this._bookmarks.length; i++) {
            if (this._bookmarks[i].anchor == b.anchor &&
                this._bookmarks[i].path == b.path) {
                return i;
            }
        }
        return false;
    }

    /** 
     * Adds a new bookmark. Bookmarks should be structured like:
     * {
     *     path: 'bookmarked/page/path',
     *     anchor: '#bookmarked-anchor',
     *     info: 'Content following bookmarked header',
     *     breadcrumbs: 'breadcrumbs/to/header'
     * }
     *
     * @param  {object}  Bookmark object
     * @return {boolean} Success or fail response
     */
    addBookmark(b) {
        // index can be 0, so check if explicitly false
        if (this.getBookmarkIndex(b) === false) {
            this._bookmarks.push(b);
            return true;
        }
        return false;
        
    }

    /** 
     * Removes saved bookmark at the specified index.
     *
     * @param  {integer} Bookmark index
     * @return {boolean} Success or fail response
     */
    removeBookmark(i) {
        try {
            this._bookmarks.splice(i, 1);
            return true;
        } catch (e) {
            return false;
        }
    }

    /** 
     * Resets the User bookmarks to nothing.
     *
     * @return {boolean} Success or fail response
     */
    resetBookmarks() {
        this._bookmarks = [];
        return true;
    }

    /** 
     * Adds a new guide status entry. Entires should be structured like:
     * {
     *     path: 'current/guide/path',
     *     title: 'Guide Title',
     *     current: {
     *         anchor: '#current-section-anchor',
     *         index: 0
     *     },
     *     complete: false
     * }
     *
     * @param  {object}  Guide status object
     * @return {boolean} Success or fail response
     */
    addGuide(g) {
        if (this.getGuideIndex(g.path) === false) {
            this._guidesStatus.push(g);
            return true;
        } else {
            return false;
        }
    }
    
    /** 
     * Gets the index value of the guide with the specified
     * path. Returns false if no guides match.
     *
     * @param  {string}  Guide pathname
     * @return {integer} Guide index in array
     */
    getGuideIndex(path) {
        for (var i = 0; i < this._guidesStatus.length; i++) {
            if (this._guidesStatus[i].path == path) {
                return i;
            }
        }
        return false;
    }
    
    /** 
     * Gets the 'current' object variable of the guide
     * with the specified path. Returns false if
     * no guides match.
     *
     * @param  {string} Guide pathname
     * @return {object} Guide current value -- {index: 0, anchor: '#abc'}
     */
    getGuideCurrent(path) {
        try {
            var index = this.getGuideIndex(path);
            return this._guidesStatus[index].current;
        } catch (e) {
            return false;
        }
    }
    
    /** 
     * Updates the 'current' object variable of the guide
     * with the specified path. Returns false if no guides
     * match.
     *
     * @param  {string}  Guide pathname
     * @param  {object}  New 'current' object variable -- {index: 0, anchor: '#abc'}
     * @return {boolean} Success or fail response
     */
    updateGuideCurrent(path, current) {
        try {
            var index = this.getGuideIndex(path);
            this._guidesStatus[index].current = current;
            return true;
        } catch (e) {
            return false;
        }
    }
    
    /** 
     * Updates the complete status of the guide to true.
     *
     * @param  {string}  Guide pathname
     * @return {boolean} Success or fail response
     */
    completeGuide(path) {
        try {
            var index = this.getGuideIndex(path);
            this._guidesStatus[index].complete = true;
            return true;
        } catch (e) {
            return false;
        }
    }
    
    /** 
     * Resets the User guide statuses to nothing.
     *
     * @return {boolean} Success or fail response
     */
    resetGuides() {
        this._guidesStatus = [];
        return true;
    }

    /** 
     * Checks if a User cookie already exists.
     *
     * @return {boolean} True or false
     */
    saveExists() {
        return getCookie('user') ? true : false;
    }

    /** 
     * Saves current User data to a cookie.
     *
     * @return {boolean} Success or fail response
     */
    save() {
        try {
            var str = JSON.stringify(this);
            var b64 = btoa(str);
            setCookie('user', b64);
            return true;
        } catch (e) {
            return false;
        }
    }

    /** 
     * Loads User data from previously saved cookie.
     *
     * @return {boolean} Success or fail response
     */
    loadSave() {
        try {
            var b64 = getCookie('user');
            var str = atob(b64);
            var json = JSON.parse(str);
            this._preferences = (json._preferences) ? json._preferences : {};
            this._bookmarks = (json._bookmarks) ? json._bookmarks : [];
            this._guidesStatus = (json._guidesStatus) ? json._guidesStatus : [];
            return true;
        } catch (e) {
            return false;
        }
    }

    /** 
     * Quickly resets a user cookie. Useful for debugging.
     *
     * @return {boolean} Success or fail response
     */
    reset() {
        this._preferences = {
            theme: "default",
            dyslexia: false
        };
        this._bookmarks = [];
        this._guidesStatus = [];
        return this.save();
    }
}

/** 
 * Create a new or update an existing cookie.
 *
 * @param {string} cname  The cookie name
 * @param {string} cvalue The value for the cookie
 */
function setCookie(cname, cvalue) {
    try {
        var d = new Date();
        d.setTime(d.getTime() + (365*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        return true;
    } catch (e) {
        return false;
    }
}

/** 
 * Get the value of a saved cookie. Returns an empty string if the cookie
 * could not be found.
 *
 * @param  {string} cname The cookie name
 * @return {string}       The value of the cookie 
 */
function getCookie(cname) {
    try {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return true;
    } catch (e) {
        return false;
    }
}
