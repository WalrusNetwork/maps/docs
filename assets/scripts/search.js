// Extracted from https://learn.cloudcannon.com/jekyll/jekyll-search-using-lunr-js/
(function() {
    function displaySearchResults(results, store) {
        var searchResults = document.getElementById("search-results");

        if (results.length) {
            // Are there any results?
            var appendString = "";

            for (var i = 0; i < results.length; i = i + 2) {
                // Iterate over the results
                appendString +=
                    '<div class="row"><div class="col-md-6"><div class="card"><div class="card-header"><h4 class="card-title"><a href="' +
                    store[results[i].ref].url +
                    '">' +
                    store[results[i].ref].collection +
                    '/' +
                    store[results[i].ref].title +
                    '</a><br></h4><h5 class="card-subtitle">Category: ' +
                    store[results[i].ref].category +
                    '</h5></div><div class="card-body"><p>' +
                    store[results[i].ref].content.substring(0, 150) +
                    '...</p></div></div></div><br>';

                if (i + 1 < results.length) {
                    appendString +=
                        '<div class="col-md-6"><div class="card"><div class="card-header"><h4 class="card-title"><a href="' +
                        store[results[i + 1].ref].url +
                        '">' +
                        store[results[i + 1].ref].collection +
                        '/' +
                        store[results[i + 1].ref].title +
                        '</a><br></h4><h5 class="card-subtitle">Category: ' +
                        store[results[i + 1].ref].category +
                        '</h5></div><div class="card-body"><p>' +
                        store[results[i + 1].ref].content.substring(0, 150) +
                        '...</p></div></div></div></div><br>';
                } else {
                    appendString += '<div class="row">';
                }

                searchResults.innerHTML =
                    "<h3>Ok! We have found " +
                    results.length +
                    " results:</h3>" +
                    appendString;
            }
        } else {
            searchResults.innerHTML =
                "<h4>Uh oh chief!</h4>\n<p>We haven't found anything.</p>";
        }
    }

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] === variable) {
                return decodeURIComponent(pair[1].replace(/\+/g, "%20"));
            }
        }
    }

    var searchTerm = getQueryVariable("query");

    if (searchTerm) {
        document.getElementById("search-input").setAttribute("value", searchTerm);

        // Initalize lunr with the fields it will be searching on. I've given title
        // a boost of 10 to indicate matches on this field are more important.
        var idx = lunr(function() {
            this.field("id");
            this.field("title", {
                boost: 10
            });
            this.field("category");
            this.field("collection");
            this.field("content");
            this.field("url");
            for (var key in window.store) {
                this.add({
                    id: key,
                    category: window.store[key].category,
                    title: window.store[key].title,
                    content: window.store[key].content,
                    url: window.store[key].url,
                    collection: window.store[key].collection
                });
            }
        });
        var results = idx.search(searchTerm);
        displaySearchResults(results, window.store);
    }
})();
