# Walrus Documentation

The Jekyll site behind the documentation of Walrus's XML configurations for our Minecraft game servers.

## Contributing

### Editing

1. Use `4 spaces` for intentation
2. First header of content pages must be `###` (these are top section headings)

### Reviewing

1. Install [Jekyll](https://jekyllrb.com/docs/)
2. Run `bundle install` from the source root
3. Run `bundle exec jekyll serve`
4. Open `localhost:4000` in your browser

### Pull Requests

1. Commits must have descriptive messages in present tense
2. Content must be grammatically correct and use US English spelling and conventions
