---
layout: facets
title: Damage
category: mechanics
order: 5
---

### Disable Damage

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<disabledamage>' | escape}}</code></td>
                <td>The filter to be executed on each damage to see if it should be applied.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- disable fall and suffocation damage -->
<disabledamage>
    <any>
        <damage>fall</damage>
        <damage>suffocation</damage>
    </any>
</disabledamage>

<!-- disable fall damage only for the 'red' team -->
<disabledamage>
    <all>
        <damage>fall</damage>
        <team>red</team>
    </all>
</disabledamage>
```

#### Block Explosion Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Element Text</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>ally</code></td>
                <td>Damage to players on the same team as the person that caused the explosion.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>self</code></td>
                <td>Damage to the person that caused the explosion.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>enemy</code></td>
                <td>Damage to players not on the same team as the person that caused the explosion.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>other</code></td>
                <td>Any other damage caused by the explosion.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- TNT damages enemies and self but not teammates -->
<disabledamage>
    <damage ally="true" self="false" enemy="false" other="false">block explosion</damage>
</disabledamage>
```

### Hunger

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="15%">Element Text</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<hunger>' | escape}}</code></td>
                <td>Specify whether hunger should depleat over time.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- disable hunger -->
<hunger>false</hunger>
```
