---
layout: facets
title: Filters
category: mechanics
order: 0
---

### Overview

Filters are true or false conditions about players, blocks, or the match in general. Filters are used by other facets to decide if certain events should occur in relation to those players, blocks, or the match.

Filters consist of **matchers** and **modifiers**. Filter matchers ask specific questions, like:

- is it made of wood?
- are they on the Red team?
- is it in the void?
- are they inside region X?

Filter modifiers can combine questions together using logic, such as:

- A and B
- A or B
- not A
- (A or B) and not (C and D)

All filters must be defined inside the main filters facet as described below.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<filters>' | escape}}</code></td>
                <td>Element containing all filters.</td>
                <td><span class="badge badge-secondary">Filters</span></td>
            </tr>
        </tbody>
    </table>
</div>

In order to use filters in other facets, the filter must have a unique ID. Top-level filters must have an ID, while those that are children to these have the option to have an ID or not.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="25%">Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the filter from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<filters>
    <!-- filter that matches only the red team -->
    <team id="only-red">red</team>
    <!-- filter that matches any of the child filters -->
    <any id="any-material">
        <material>stone</material>
        <!-- children can have an id if needed -->
        <material id="only-wood">wood</material>
    </any>
    <!-- can reference existing filters by their id -->
    <all id="only-red-wood">
        <filter id="only-red"/>
        <filter id="only-wood"/>
    </all>
</filters>
```

### Filter Logic

Filters have 3 different states: **allow**, **deny**, or **abstain**. When a filter allows, it will permit the specific event to occur, such as a player breaking a block. However, when a filter denies, it will prevent the event from being carried out.

Abstaining occurs when the filter doesn't make sense in the context it is being used in. For instance, you can't ask if a material is on the Red team, or if a player is made of wood. When this happens, the filter will abstain from that decision and things will behave as if the filter didn't exist. This is particuarly useful when applying multiple rules of the same time, such as block-place, to the same region.

Filters can be forced to abstain in certain contexts with the [allow](#allow) and [deny](#deny) filter modifiers.

### Filter Matchers

#### Game

Game filters match specific game states for the running match.

##### <code>{{'<time>' | escape}}</code>

The time filter returns allow when the elapsed match time is true in comparison with the supplied duration and comparator option. Requires a duration as the element text.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>compare</code></td>
                <td>The number comparator option to use when evaluating the supplied duration to the elapsed time.</td>
                <td><span class="badge badge-primary">Number Comparator</span></td>
                <td>greater than equal</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- return allow after 5 minutes into the match, otherwise deny -->
<time>5m</time>

<!-- return allow while match time is less than 8 minutes, otherwise deny -->
<time compare="less than equal">8m</time>
```

#### Player

##### <code>{{'<team>' | escape}}</code>

The team filter returns allow when the target player is a member of the specified team. Requires a team ID as the element text.

```xml
<!-- return allow when the player is on 'red' team, otherwise deny -->
<team>red</team>
```

##### <code>{{'<carrying>' | escape}}</code>
##### <code>{{'<holding>' | escape}}</code>
##### <code>{{'<wearing>' | escape}}</code>
##### <code>{{'<flying/>' | escape}}</code>

The flying filter returns allow when the target player is flying. This filter can have an optional state attribute which specifies if the filter should be testing for the true or false state of the player. If true, the filter will allow if the player _is_ flying. However, if you set this to false, the filter will allow if the player _is not_ flying.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>state</code></td>
                <td>The state in which to test this filter.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

##### <code>{{'<sprinting/>' | escape}}</code>

The sprinting filter returns allow when the target player is sprinting. This filter can have an optional state attribute which specifies if the filter should be testing for the true or false state of the player. If true, the filter will allow if the player _is_ sprinting. However, if you set this to false, the filter will allow if the player _is not_ sprinting.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>state</code></td>
                <td>The state in which to test this filter.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

##### <code>{{'<sneaking/>' | escape}}</code>

The sneaking filter returns allow when the target player is sneaking. This filter can have an optional state attribute which specifies if the filter should be testing for the true or false state of the player. If true, the filter will allow if the player _is_ sneaking. However, if you set this to false, the filter will allow if the player _is not_ sneaking.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>state</code></td>
                <td>The state in which to test this filter.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

##### <code>{{'<on-ground/>' | escape}}</code>

The on ground filter returns allow when the target player is fully grounded on a block. This filter can have an optional state attribute which specifies if the filter should be testing for the true or false state of the player. If true, the filter will allow if the player _is_ on the ground. However, if you set this to false, the filter will allow if the player _is not_ on the ground.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>state</code></td>
                <td>The state in which to test this filter.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

##### <code>{{'<objective>' | escape}}</code>

The objective filter returns allow when the target player has completed a match objective.

<div class="callout callout-warning">The objective filter only works in contexts where there is a player involved. Objective filters also only currently work with <a href="{{ site.baseurl }}/facets/hills">Hills</a>.</div>

```xml
<!-- return true when the player, or player's team, owns the 'middle-hill', otherwise deny -->
<objective>middle-hill</objective>
```

#### Material

##### <code>{{'<item>' | escape}}</code>
##### <code>{{'<material>' | escape}}</code>

#### Spatial

##### <code>{{'<inside/>' | escape}}</code>

The inside filter returns allow if the target player is inside the specified region.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="35%">Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>region</code></td>
                <td>The region to test whether a player is inside of.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Region</a></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- return allow when the player is inside the 'super-region', otherwise deny -->
<inside region="super-region"/>
```

##### <code>{{'<void/>' | escape}}</code>

The void filter returns allow when there is air at the specified Y levels for where the event takes place. You can optionally change where the void filter checks for air blocks, or provide it with a list of materials that it should ignore when checking for void. The void filter is most commonly used with [Applicators]({{ site.baseurl }}/facets/applicators) to prevent building out in the void away from the map.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>min</code></td>
                <td>The lower Y level to check for air.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>max</code></td>
                <td>The upper Y level to check for air.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>ignored-blocks</code></td>
                <td>The blocks to ignore and treat as empty when checking for air in the specified Y levels.</td>
                <td><span class="badge badge-primary">Multi Material Matcher</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- return allow if there is air at Y level 0, otherwise deny -->
<void/>

<!-- return allow if there is air at Y level 0 to 5, ignoring bedrock, otherwise deny -->
<void max="5" ignored-blocks="bedrock"/>
```

#### Event

##### <code>{{'<actor>' | escape}}</code>
##### <code>{{'<damage>' | escape}}</code>
##### <code>{{'<random>' | escape}}</code>
##### <code>{{'<sometimes/>' | escape}}</code>

#### Competitor

##### <code>{{'<rank>' | escape}}</code>
##### <code>{{'<score>' | escape}}</code>

#### Entity

##### <code>{{'<entity>' | escape}}</code>
##### <code>{{'<explosion/>' | escape}}</code>
##### <code>{{'<spawn-reason>' | escape}}</code>

#### Static

Static filters are unchanging and will always return the same result no matter the timing or context.

##### <code>{{'<always/>' | escape}}</code>

The always filter matches anything and everything. It will always return true no matter the context.

```xml
<!-- always allow -->
<always/>
```

##### <code>{{'<never/>' | escape}}</code>

The never filter matches absolutely nothing. It will always return false no matter the context.

```xml
<!-- never allow -->
<never/>
```

### Filter Modifiers

#### Logic

Logic filter modifiers change how filters are interpreted either individually, or together as a collection of filters.

##### <code>{{'<all>' | escape}}</code>

The all filter modifier returns allow when **all** of the child filters return allow, otherwise deny.

```xml
<!-- allow when the player is on the 'red' team and 5 minutes has elapsed -->
<all id="all-must-match">
    <team>red</team>
    <time>5m</time>
</all>
```

##### <code>{{'<any>' | escape}}</code>

The any filter modifier returns allow when **any** of the child filters return allow, otherwise deny.

```xml
<!-- allow the 'red' team or anyone holding the Magic Stick -->
<any id="any-must-match">
    <team>red</team>
    <holding>
        <item material="stick" name="Magic Stick"/>
    </holding>
</any>
```

##### <code>{{'<not>' | escape}}</code>

The not filter modifier returns deny when the child filter returns allow, otherwise allow. The not filter modifier only accepts a **single child**. Use another logical modifier such as all or any in order to combine multiple filters into a single child.

```xml
<!-- deny when something happens in the void -->
<not id="not-void">
    <void/>
</not>
<!-- deny 'red' team from interacting with tnt -->
<not id="not-red-tnt">
    <all>
        <team>red</team>
        <material>tnt</material>
    </all>
</not>
```

#### Abstention

Abstention filter modifiers allow filters to abstain when the child filters return anything other the allow.

##### <code>{{'<deny>' | escape}}</code>

The deny filter modifier returns deny when the child filters return allow, otherwise abstains. You would use `<deny>` over `<not>` when you are trying to **deny multiple of the same type of events from occurring in the same region**, such as block rules, but for different reasons. Applicators are read from top to bottom and an event is tested against each of them to see if it should be allowed to occur. Each applicator is tested until one returns either allow or deny, which then determines if the event is carried out. With the `<deny>` filter modifier, the applicator will either return deny or be ignored.

In the example below, we first check if the block being place is a wool material. If it is, we deny it from being placed. If it isn't, that applicator rule is ignored and we go to the next one. However, if the `deny-safety` filter used `<not>` instead of `<deny>`, things would play out differently. It would still check if the material is wool and deny it being placed if it is, but the difference is that it will _allow_ everything else and this is where the applicator checks stop. The `not-void` check doesn't get run and blocks will be able to be placed over the void.

```xml
<filters>
    <deny id="deny-safety">
        <material>wool</material>
    </deny>
    <not id="not-void">
        </void>
    </not>
</filters>
<applicators>
    <apply region="everywhere" block-place="deny-safety" message="{region.error.place.safety-wool}"/>
    <apply region="everywhere" block-place="not-void" message="{region.error.modify.void}"/>
</applicators>
```

##### <code>{{'<allow>' | escape}}</code>

The allow filter modifier returns allow when the child filters return allow, otherwise abstains. You would use `<allow>` as a parent to other filters when you are trying to **allow multiple of the same type of events occurring in the same region**, but for different reasons.

```xml
<!-- allow example -->
```

#### Query

##### <code>{{'<attacker>' | escape}}</code>
##### <code>{{'<victim>' | escape}}</code>

### Extended Examples

### Old Reference

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text/Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3"><strong>Game</strong> <small>(match game states)</small></td>
            </tr>
            <tr>
                <td><code>{{'<always/>' | escape}}</code></td>
                <td>Matches everything (always allows).</td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<never/>' | escape}}</code></td>
                <td>Matches nothing (always denies).</td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<time>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when the elapsed time is true in comparison with the supplied duration and comparator option.</td>
                <td><a href="#">Duration</a></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Player</strong> <small>(match a player)</small></td>
            </tr>
            <tr>
                <td><code>{{'<team>' | escape}}</code>/<code>{{'<group>' | escape}}</code></td>
                <td>Matches when a player is on the specified team.</td>
                <td><a href="#">Team ID</a></td>
            </tr>
            <tr>
                <td><code>{{'<carrying>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when a player is carrying the specified item.</td>
                <td><a href="#">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<holding>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when a player is holding the specified item.</td>
                <td><a href="#">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<wearing>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when a player is wearing the specified item.</td>
                <td><a href="#">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<flying/>' | escape}}</code></td>
                <td>Matches when a player is flying.</td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<sprinting/>' | escape}}</code></td>
                <td>Matches when a player is sprinting.</td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<sneaking/>' | escape}}</code></td>
                <td>Matches when a player is sneaking.</td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<on-ground/>' | escape}}</code></td>
                <td>Matches when a player is on the ground.</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Materials</strong> <small>(match specific materials)</small></td>
            </tr>
            <tr>
                <td><code>{{'<item>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when the specified item is involved in the event.</td>
                <td><a href="#">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<material>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when the specified material is involved in the event.</td>
                <td><a href="#">Material Matcher</a></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Spatial</strong> <small>(match physical locations)</small></td>
            </tr>
            <tr>
                <td><code>{{'<inside>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when a player is inside the specified region.</td>
                <td><a href="#">Region</a></td>
            </tr>
            <tr>
                <td><code>{{'<void/>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when the blocks within y plane of the specified range are air or ignored.</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Event</strong> <small>(match transient events)</small></td>
            </tr>
            <tr>
                <td><code>{{'<actor>' | escape}}</code></td>
                <td></td>
                <td><a href="#">Entity ID</a></td>
            </tr>
            <tr>
                <td><code>{{'<cause>' | escape}}</code></td>
                <td></td>
                <td><a href="#">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<random>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches randomly based on the specified amount of randomness.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<sometimes/>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches only 50% of the time.</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Damage</strong> <small>(match damage/combat events)</small></td>
            </tr>
            <tr>
                <td><code>{{'<damage>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when the specified damage time is the type of damage a player is receiving.</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Competitor</strong> <small>(match teams or ffa players)</small></td>
            </tr>
            <tr>
                <td><code>{{'<rank>' | escape}}</code></td>
                <td>Matches when the competitor's rank is within the specified range.</td>
                <td><span class="badge badge-primary">Number Range</span></td>
            </tr>
            <tr>
                <td><code>{{'<score>' | escape}}</code></td>
                <td>Matches when the competitor's score is within the specified range.</td>
                <td><span class="badge badge-primary">Number Range</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Entity</strong> <small>(match an entity)</small></td>
            </tr>
            <tr>
                <td><code>{{'<entity>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when the specified entity is causing an event.</td>
                <td><a href="#">Entity ID</a></td>
            </tr>
            <tr>
                <td><code>{{'<explosion/>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when an entity is something that exploded.</td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<spawn>' | escape}}</code>/<code>{{'<spawn-reason>' | escape}}</code></td>
                <td><a href="#" class="table-ref-link fas fa-chevron-down"></a> Matches when the specified spawn reason is the reason an entity was spawned.</td>
                <td><a href="#">Spawn Reason</a></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2"><strong>Logic</strong> <small>(combine filters)</small></td>
            </tr>
            <tr>
                <td><code>{{'<all>' | escape}}</code></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<any>' | escape}}</code></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<not>' | escape}}</code></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Abstention</strong> <small>(force filters to abstain)</small></td>
            </tr>
            <tr>
                <td><code>{{'<allow>' | escape}}</code></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<deny>' | escape}}</code></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Query</strong> <small>(change the question)</small></td>
            </tr>
            <tr>
                <td><code>{{'<attacker>' | escape}}</code></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<victim>' | escape}}</code></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>
