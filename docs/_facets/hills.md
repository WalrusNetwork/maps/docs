---
layout: facets
title: Hills
category: games
order: 2
---

### Overview

Hills refers to both the _King of the Hill_ and _Domination_ games. Both King of the Hill and Domination use the same XML configuration, however the differences in how the plugin interprets the hills is defined by the `<game>..</game>` previously defined. King of the Hill, using the `koth` game type, has competitors capturing hills in order to earn points towards a point limit or to have the most points after a defined amount of time. Domination, using the `dom` game type, has competitors fight to be the first to capture each of the hills, typically in a sequential order, or to have the most points under their control by the end of the time limit.

<div class="callout callout-info">King of the Hill and Domination are mutually exclusive and cannot be combined in the same map.</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<hills>' | escape}}</code></td>
                <td>Element containing all hill objectives.</td>
                <td><span class="badge badge-secondary">Hills Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<hill>' | escape}}</code></td>
                <td>A single hill objective.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<hill>' | escape}}</code> Attributes

In order to reference a hill objective in other facets, such as [Filters]({{ site.baseurl }}/facets/filters), the hill must have a unique ID name.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="45%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default (koth)</th>
                <th width="10%">Default (dom)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the hill from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td><span class="badge badge-danger">Required</span> The name of the hill.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><code>capture-time</code></td>
                <td>The amount of time it takes to control the hill.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td>30s</td>
                <td>30s</td>
            </tr>
            <tr>
                <td><code>capture-region</code></td>
                <td><span class="badge badge-danger">Required</span> The region where the hill must be captured.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><code>progress-region</code></td>
                <td><span class="badge badge-danger">Required</span> The region where the capture progress will be displayed</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><code>control-region</code></td>
                <td>The region where the hill owner will be displayed when the hill is captured.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><code>initial-owner</code></td>
                <td>The team owning the control point when the match starts.</td>
                <td><a href="{{ site.baseurl }}/facets/teams">Team</a></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><code>capture-rule</code></td>
                <td><a href="#capture-rules" class="table-ref-link fas fa-chevron-down"></a> The rule used to decide who is in control of the hill at any given moment when contested.</td>
                <td><a href="#capture-rule">Capture Rule</a></td>
                <td>majority</td>
                <td>exclusive</td>
            </tr>
            <tr>
                <td><code>neutral-state</code></td>
                <td>Specify if the hill must be un-captured before it can be captured again.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>permanent</code></td>
                <td>Specify if the hill can only be captured once.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>capture-filter</code></td>
                <td>The teams that can capture the hill. Teams that don't match this filter can still prevent other teams from capturing by standing on the hill. They can also uncapture the hill if neutral-state is enabled.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>always</td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>player-filter</code></td>
                <td>The players who can capture this hill. Players who don't match this filter cannot affect the hill in any way.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>always</td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>recovery</code></td>
                <td>Find a better way to describe this.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>oo</td>
                <td>1</td>
            </tr>
            <tr>
                <td><code>decay</code></td>
                <td>Find a better way to describe this.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>oo</td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>time-multiplier</code></td>
                <td>Adjust capture time relative to the amount of team players on the control point.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td>0</td>
                <td>0.1</td>
            </tr>
            <tr>
                <td><code>visual-materials</code></td>
                <td><a href="#visual-materials" class="table-ref-link fas fa-chevron-down"></a> The materials that are modified when updating the progress regions.</td>
                <td><span class="badge badge-primary">Multi Material Matcher</span></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Capture Rules

The capture rule defines the logic used to decide which team is in control of the hill. Values for `capture-rule` can be one of the following:

 - `exclusive` The team must be the only team with players on the hill. Default for Domination.
 - `majority` The team must have more players on the hill than all other teams combined. Default for King of the Hill.
 - `lead` The team must have more players on the hill than any other single team.

Players who match either `capture-filter` or `player-filter` are counted when calculating the `capture-rule`. Players who don’t match either filter cannot affect the hill at all.

#### Capture Regions

Capture progress is displayed inside the defined `progress-region` using the dominating teams color. After a control point is captured, the `control-region` is also filled with that color. The progress and captured regions must be bounded regions, e.g. cylinders & cuboids will work, circles and rectagnles will not. Only the default visual materials or materials defined in visual-materials will be modified in the progress regions when they are updated.

#### Visual Materials

Visual materials inside of a hill's `progress-region` or `control-region` will change color to represent the team that is controlling the hill. These materials will return to their normal color when the hill is in a neutral state. Visual materials include `wool`, `carpet`, `stained clay`, `stained glass`, and `stained glass panes`.

### King of the Hill

King of the Hill has competitors capturing hills in order to earn points towards a point limit or to have the most points after a defined amount of time. In order to use the King of the Hill functionality of hills, you must include `<game>koth</game>` at the beginning of the map's XML.

King of the Hill must be combined with the [score facet]({{ site.baseurl }}/facets/scoring) in order to award points to competitors. While you can also make use of kill and death points, you are unable to combine King of the Hill with scoreboxes.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="45%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>points</code></td>
                <td>The amount of points awarded per second for owning the hill.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>1</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<game>koth</game>
<score limit="500"/>
<hills>
    <hill id="point-a" name="Point A" capture-time="2s" points="1" capture-region="point-a-base" progress-region="point-a-base" control-region="point-a-indicator" initial-owner="blue"/>
    <hill id="point-b" name="Point B" capture-time="5s" points="2" capture-region="point-b-base" progress-region="point-b-base" control-region="point-b-indicator"/>
    <hill id="point-c" name="Point C" capture-time="2s" points="1" capture-region="point-c-base" progress-region="point-c-base" control-region="point-c-indicator" initial-owner="red"/>
</hills>
<regions>
    <cylinder id="point-a-base" base="-341.5,6,-67.5" radius="3" height="8"/>
    <cylinder id="point-b-base" base="-334.5,8,-29.5" radius="3" height="5"/>
    <cylinder id="point-c-base" base="-327.5,6,8.5" radius="3" height="8"/>
    <cylinder id="point-a-indicator" base="-341.5,33,-67.5" radius="3" height="8"/>
    <cylinder id="point-b-indicator" base="-334.5,33,-29.5" radius="3" height="8"/>
    <cylinder id="point-c-indicator" base="-327.5,33,8.5" radius="3" height="8"/>
</regions>
```

### Domination

Domination has competitors fight to be the first to capture each of the points, typically in a sequential order, or to have the most points under their control by the end of the time limit. In order to use the Domination functionality of hills, you must include `<game>dom</game>` at the beginning of the map's XML.

For the sequential aspect of Domination to work correctly, hills must be defined in the order that they must be captured. For example, Team A will capture the hills in order from top to bottom, while Team B will capture them from bottom to top. Sequential hills can only work when there are only 2 teams competing.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="45%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>sequential</code></td>
                <td>Specify if the hills can only be captured in sequential order.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>sequential-owner</code></td>
                <td>The first hill that must be captured by the team to initiate the sequential order. Only required if hills do not have an initial owner.</td>
                <td><a href="{{ site.baseurl }}/facets/teams">Team</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>overtime</code></td>
                <td></td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<game>dom</game>
<hills capture-time="10s">
    <hill id="red-last" name="Red Last" capture-region="red-last-capture" progress-region="red-last-capture" initial-owner="red"/>
    <hill id="red-2nd" name="Red 2nd" capture-region="red-2nd-capture" progress-region="red-2nd-capture" control-region="red-2nd-control"/>
    <hill id="mid" name="Mid" capture-region="mid-capture" progress-region="mid-capture" control-region="mid-control"/>
    <hill id="blue-2nd" name="Blue 2nd" capture-region="blue-2nd-capture" progress-region="blue-2nd-capture" control-region="blue-2nd-control"/>
    <hill id="blue-last" name="Blue Last" capture-region="blue-last-capture" progress-region="blue-last-capture" initial-owner="blue"/>
</hills>
```
