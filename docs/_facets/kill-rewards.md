---
layout: facets
title: Kill Rewards
category: gear
order: 4
---

### Overview

Kill rewards allow you to reward players with additional kit elements when they kill another player. This can, for example, be used to provide item updates or buffs to players with the more kills they get during the match.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<kill-rewards>' | escape}}</code></td>
                <td>Element containing all kill rewards.</td>
                <td><span class="badge badge-secondary">Kill Reward Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<reward>' | escape}}</code></td>
                <td>A single reward.</td>
                <td><span class="badge badge-secondary">Reward Sub-elements</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<reward>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>filter</code></td>
                <td>Filter who can claim this reward and when.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>after</code></td>
                <td>Reward the player after a number of consecutive kills without dying.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>every</code></td>
                <td>Reward the player after every number kills.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>kit</code></td>
                <td>A pre-defined kit to reward the player with.</td>
                <td><a href="{{ site.baseurl }}/facets/kits">Kit</a></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<reward>' | escape}}</code> Sub-elements

Rewards are treated as kits that will be non-forcefully applied to the player when they get a kill, or reach a certain amount of kills. Each reward sub-element can be any kit element described on the <a href="{{ site.baseurl }}/facets/kits">kits</a> page.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Reward Sub-elements</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href="{{ site.baseurl }}/facets/kits">Kit Elements</a></td>
                <td>An kit element given as a reward.</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<kill-rewards>
    <!-- reward to give every kill -->
    <reward>
        <item material="golden apple"/>
        <item amount="16" material="wood"/>
    </reward>
    <!-- after 5 consecutive kills give the player an iron sword and speed -->
    <reward after="5">
        <item material="iron sword"/>
        <effect duration="12">speed</effect>
    </reward>
    <!-- reference a pre-defined kit to give as a reward -->
    <reward after="10" kit="super-kit"/>
</kill-rewards>
```
