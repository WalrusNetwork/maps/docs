---
layout: facets
title: Miscellaneous
category: mechanics
order: 10
---

### Overview

The following are miscellaneous mechanics that can be configured globally for the match. These facets will affect all players in the match.

#### <code>{{'<timelock>' | escape}}</code>

Specifies if the time cycle should be locked to the current world time. Defaults to `false`.

```xml
<timelock>true</timelock>
```

#### <code>{{'<hunger>' | escape}}</code>

Specifies if players should have their hunger meter depleat. Defaults to `true`. <br>
Should always be set to `false` in conquest style games.

```xml
<hunger>false</hunger>
```
