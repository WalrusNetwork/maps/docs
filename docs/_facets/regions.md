---
layout: facets
title: Regions
category: mechanics
order: 1
---

### Overview

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<regions>' | escape}}</code></td>
                <td>Element containing all regions.</td>
                <td><span class="badge badge-secondary">Regions</span></td>
            </tr>
        </tbody>
    </table>
</div>

In order to use regions in other facets, the region must have a unique ID. Top-level regions must have an ID, while those that are children to these have the option to have an ID or not.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="25%">Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the region from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### Properly Defining Regions



### Bounded Regions

Bounded Region types are regions which have finite volumes and are limited in their size. Bounded regions are also randomizable, meaning that facets that use these regions to move or place something, such as spawns or portals, will randomly pick a point within the bounds of the region to use.

#### <code>{{'<cuboid/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>min</code></td>
                <td><span class="badge badge-danger">Required</span> The lower bounding coordinate for the cuboid.</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>max</code></td>
                <td><span class="badge badge-danger">Required</span> The upper bounding coordinate for the cuboid.</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<cuboid min="0,0,0" max="10,10,10"/>
<!-- min doesn't strictly have to be less than max -->
<cuboid min="25.5,10,50.5" max="10,0,5"/>
```

#### <code>{{'<box/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>center</code></td>
                <td><span class="badge badge-danger">Required</span> The central coordinate of the box..</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>x</code></td>
                <td>The length of the box on the X coordinate.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>y</code></td>
                <td>The height of the box on the Y coordinate.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>z</code></td>
                <td>The length of the box on the Z coordinate.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- a box region covering x1,y1,z1 to x2,y2,z2 -->
<box center="0,10,00" x="10" y="3" z="10"/>
```

#### <code>{{'<cylinder/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>base</code></td>
                <td><span class="badge badge-danger">Required</span> The centered base of the cylinder.</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>radius</code></td>
                <td><span class="badge badge-danger">Required</span> The radius of the cylinder.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>height</code></td>
                <td><span class="badge badge-danger">Required</span> The height of the cylinder.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<cylinder base="0,0,0" radius="5" height="10"/>
```

#### <code>{{'<sphere/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>origin</code></td>
                <td><span class="badge badge-danger">Required</span> The central point of the sphere.</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>radius</code></td>
                <td><span class="badge badge-danger">Required</span> The radius of the sphere.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<sphere origin="0.5,50,0.5" radius="10"/>
```

#### <code>{{'<block>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<block>' | escape}}</code></td>
                <td>A single block in the Minecraft world.</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<block>2,10,2</block>
```

#### <code>{{'<point>' | escape}}</code>

Despite being a randomizable region, a point will always return the exact same location.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<point>' | escape}}</code></td>
                <td>A single point in the Minecraft world.</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<point>2.5,10.5,2.5</point>
```

### Unbounded Regions

Unbounded Regions, unlike Bounded Regions, have infinite volumes and typically occupy all blocks within a given direction. Unbounded Regions are not randomizable and can not be used with facets other than [Applicators]({{ site.baseurl }}/facets/applicators).

#### <code>{{'<rectangle/>' | escape}}</code>

Rectangles occupy all the vertical space between two coordinates.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>min</code></td>
                <td><span class="badge badge-danger">Required</span> The lower bounding coordinate for the rectangle.</td>
                <td><span class="badge badge-primary">2D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>max</code></td>
                <td><span class="badge badge-danger">Required</span> The upper bounding coordinate for the rectangle.</td>
                <td><span class="badge badge-primary">2D Vector</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml

```

#### <code>{{'<circle/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>center</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>radius</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml

```

#### <code>{{'<sector/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>x</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>z</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>start</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>end</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml

```
#### <code>{{'<above/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>x</code></td>
                <td></td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>y</code></td>
                <td></td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>z</code></td>
                <td></td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- everything above y 50 -->
<above y="50"/>
<!-- everything above x and z 10 -->
<above x="10" z="10"/>
```

#### <code>{{'<below/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>x</code></td>
                <td></td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>y</code></td>
                <td></td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>z</code></td>
                <td></td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- everything below y 50 -->
<below y="50"/>
<!-- everything below x and z 10 -->
<below x="10" z="10"/>
```

### Static Regions

#### <code>{{'<everywhere/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<everywhere/>' | escape}}</code></td>
                <td>A reference-able infinite size region, contains everything.</td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<nowhere/>' | escape}}</code>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<nowhere/>' | escape}}</code></td>
                <td>A reference-able zero size/position region, contains nothing.</td>
            </tr>
        </tbody>
    </table>
</div>

### Region Modifiers

<div class="row mb-3">
    <div class="col-lg-8">
        Region Modifiers change how regions can be interpreted in complex and helpful ways. Each of the region modifiers accept one or multiple region definitions and children to which they will act upon. Consider the following 3 regions in the region modifiers below:
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/base-regions.png" width="75%" class="mx-auto d-block">
    </div>
</div>

#### <code>{{'<union>' | escape}}</code>

<div class="row mb-3">
    <div class="col-lg-8">
        The <code>{{'<union>' | escape}}</code> region modifier, or alternatively <code>{{'<join>' | escape}}</code>, combines all the child regions into a single region. This will allow you to treat different regions exactly the same in certain contexts, such as team bases, or create complex shapes not possible with individual regions alone. Unions are treated as <i>Bounded Regions</i> as long as all the children regions are bounded regions themselves.
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/union.png" width="75%" class="mx-auto d-block">
    </div>
</div>

```xml
<union id="unioned-regions">
    <region id="region-1"/>
    <region id="region-2"/>
    <region id="region-3"/>
</union>
```

#### <code>{{'<negative>' | escape}}</code>

<div class="row mb-3">
    <div class="col-lg-8">
        The <code>{{'<negative>' | escape}}</code> region modifier, or alternatively <code>{{'<invert>' | escape}}</code>, takes the inverse of all the child regions. This allows you to select everywhere in the world <i>except</i> in the specific regions which are provided. Negative regions are always treated as <i>Unbounded Regions</i>.
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/negative.png" width="75%" class="mx-auto d-block">
    </div>
</div>

```xml
<negative id="negative-region">
    <region id="region-1"/>
</negative>
```

#### <code>{{'<intersect>' | escape}}</code>

<div class="row mb-3">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/intersect.png" width="75%" class="mx-auto d-block">
    </div>
</div>

```xml
<intersect id="intersected-regions">
    <region id="region-1"/>
    <region id="region-2"/>
</intersect>
```

#### <code>{{'<complement>' | escape}}</code>

<div class="row mb-3">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/complement.png" width="75%" class="mx-auto d-block">
    </div>
</div>

```xml
<complement id="complement-regions">
    <region id="region-1"/>
    <region id="region-2"/>
    <region id="region-3"/>
</complement>
```

#### <code>{{'<translate/>' | escape}}</code>

<div class="row mb-3">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/translate.png" width="75%" class="mx-auto d-block">
    </div>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region to translate by the offset.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>offset</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<translate id="translated-region" region="region-3" offset="0,0,10"/>
```

#### <code>{{'<mirror/>' | escape}}</code>

<div class="row mb-3">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/mirror.png" width="75%" class="mx-auto d-block">
    </div>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region to mirror over the origin.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>origin</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>normal</code></td>
                <td><span class="badge badge-danger">Required</span> </td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<mirror id="mirrored-region" region="region-3" origin="0,0,0" normal="0,0,1"/>
```

#### <code>{{'<bounds/>' | escape}}</code>

<div class="row mb-3">
    <div class="col-lg-8">
        A region which selects the area around the region specified. Similar to the WorldEdit <code>//walls</code> command.
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/bounds.png" width="75%" class="mx-auto d-block">
    </div>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region to create the bounds around.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>x-axis</code></td>
                <td></td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>y-axis</code></td>
                <td></td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>z-axis</code></td>
                <td></td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<bounds id="region-bounds" region="region-1"/>
```
