---
layout: facets
title: Portals
category: mechanics
order: 4
---

### Overview

Portals teleport a player that enters the specified region by a certain offset amount or to a random location inside of a bounded region.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<portals>' | escape}}</code></td>
                <td>Element containing all portals.</td>
                <td><span class="badge badge-secondary">Portals Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<portal>' | escape}}</code></td>
                <td>A single portal.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<portal>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region where the portal enterance is located.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>destination</code></td>
                <td>The detination of the portal. Becomes the reverse portal enterance if the portal is <code>bidirectional</code>. Cannot be combined with the <code>x</code>, <code>y</code>, or <code>z</code> attributes.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>x</code></td>
                <td>The number of blocks to offset the player's <code>x</code> position by. Cannot be combined with the <code>destination</code> attribute.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>y</code></td>
                <td>The number of blocks to offset the player's <code>y</code> position by. Cannot be combined with the <code>destination</code> attribute.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>z</code></td>
                <td>The number of blocks to offset the player's <code>z</code> position by. Cannot be combined with the <code>destination</code> attribute.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>filter</code></td>
                <td>Filter to determine who can use this portal.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>yaw</code></td>
                <td>The direction (left,right) the player will be looking when teleported.</td>
                <td><span class="badge badge-primary">Number</span> / <span class="badge badge-primary">Direction</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>pitch</code></td>
                <td>The direction (up,down) the player will be looking when teleported.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>look</code></td>
                <td>The location the player will be looking at when teleported.</td>
                <td><span class="badge badge-primary">3D Vectior</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>observers</code></td>
                <td>Specify if Observers can use the portal.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>bidirectional</code></td>
                <td>Makes the portal <code>destination</code> a reverse portal enterance allowing the player to teleport back.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>sound</code></td>
                <td>Specify if a sound should be played on portal use.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>smooth</code></td>
                <td>Specify if the player should be teleported smoothly.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<portals>
    <!-- two way portal at 'portal-above' and 'portal-below' -->
    <portal region="portal-above" destination="portal-below" bidirectional="true" yaw="north"/>
    <!-- add 100 to the players X position - everything else remains unchanged -->
    <portal region="portal-enterance" x="100"/>
</portals>
```
