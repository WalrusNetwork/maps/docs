---
layout: facets
title: Protocol
category: getting-started
order: 2
---

### Overview

The protocol specifies which version of the Game Manager plugin that the map is designed for. Maps using protocols older than the current may interpret features in outdated ways. New maps should always use the most recent protocol. The current protocol is {{site.current_proto}}.

```xml
<map proto="{{site.current_proto}}">
<!-- map facets -->
</map>
```

### Protocol 2.0.0

Initial protocol for Walrus' plugin, Game Manager. See [guide](#) for the differences between Project Ares and Game Manager.
