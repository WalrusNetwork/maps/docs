---
layout: facets
title: Projectiles
category: gear
order: 3
---

### Custom Projectiles

Not yet supported.

### Bow Projectiles

Bows can be modified to shoot a different projectile at a custom speed. The higher the velocity of the entity being fired, the more damage it will deal to a player that it hits. Additionally, potion effects can be applied to the victim when they are hit with a modified projectile.

Modifing bow projectiles affects _all_ bows in the match. You are unable to have only some bows affected by the modification.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<modify-bow-projectile>' | escape}}</code></td>
                <td>Node containing all the bow projectile modification rules.</td>
                <td><span class="badge badge-secondary">Bow Projectile Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Sub-elements</strong></td>
                <td><strong>Element Text</strong></td>
            </tr>
            <tr>
                <td><code>{{'<projectile>' | escape}}</code></td>
                <td>The entity to use as the bow projectile.</td>
                <td><a href="{{ site.baseurl }}/references/entity-types">Entity</a></td>
            </tr>
            <tr>
                <td><code>{{'<velocity>' | escape}}</code></td>
                <td>The velocity modifier of the bow projectile.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<effect>' | escape}}</code></td>
                <td>A potion effect to apply to players hit by the bow projectile. Can use multiple effects.</td>
                <td><a href="{{ site.baseurl }}/references/entity-types">Effect</a></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- shoot ender peals from the bow and inflict blindness to players on contact -->
<modify-bow-projectile>
    <projectile>EnderPearl</projectile>
    <effect duration="5">blindness</effect>
</modify-bow-projectile>
```

```xml
<!-- give fired arrows a poison effect -->
<modify-bow-projectile>
    <effect duration="12">poison</effect>
</modify-bow-projectile>
```
