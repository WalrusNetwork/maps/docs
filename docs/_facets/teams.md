---
layout: facets
title: Teams
category: format
order: 0
---

### Overview

<div class="callout callout-danger">This format cannot be used in conjunction with the <a href="{{ site.baseurl }}/facets/players">Players</a> format.</div>

Teams are a type of competitor in a match.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<teams>' | escape}}</code></td>
                <td>The teams node, containing all the individual teams.</td>
                <td><span class="badge badge-secondary">Team Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Sub-elements</strong></td>
                <td><strong>Element Text</strong></td>
            </tr>
            <tr>
                <td><code>{{'<team>' | escape}}</code></td>
                <td>A single team node containing the teams name.</td>
                <td><span class="badge badge-primary">String</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<team>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td><span class="badge badge-danger">Required</span> Unique identifier used to reference the team from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>color</code></td>
                <td><span class="badge badge-danger">Required</span> The team's display color.</td>
                <td><a href="#">Chat Color</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>min</code></td>
                <td>Required amount of players for this team.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>max</code></td>
                <td><span class="badge badge-danger">Required</span> Maximum players for this team, normal players cannot join the team once it reaches this size.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>max-overfill</code></td>
                <td>Maximum players hard limit for this team, nobody can join the team once this limit is reached. Must be greater than the defined <code>max</code>.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>max*1.05</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<teams>
   <team id="red-team" color="red" max="10">Red</team>
   <team id="blue-team" color="blue" max="10">Blue</team>
</teams>
```
