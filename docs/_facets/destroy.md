---
layout: facets
title: Destroy
category: games
order: 1
---

### Overview

Destroy refers to both the _Destroy the Core_ and _Destroy the Monument_ games. These games can be used in combination with each other to create a _Destroy the Core(s) and Monument(s)_ style map. The Destroy game genre consists of teams attempting to destroy a specific structure, generally in ememy territory, while defending their own simultaneously.

### Destroy the Core

Destroy the Core is a game where teams must break a hole in a core-like structure in enemy territory to have the containing liquid leak out a certain distance. Traditionally cores are filled with lava, but can contain any liquid. Be aware that this liquid should not be found anywhere else on the map as it could falsely trigger the core as being leaked if it comes too close to the core region. The containing liquid is automatically recongnized. Destroy the Core uses the `<game>dtc</game>` game tag.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<cores>' | escape}}</code></td>
                <td>Element containing all core objectives.</td>
                <td><span class="badge badge-secondary">Cores Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<core>' | escape}}</code></td>
                <td>A single core objective.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<core>' | escape}}</code> Attributes

In order to reference a core objective in other facets, such as [Filters]({{ site.baseurl }}/facets/filters), the core must have a unique ID name. A core can be composed of any combination of materials, with the most common being obsidian. Before a core is counted as completed, the containig liquid must leak a certain distance, defined by the `leak` attribute, below the bottom of the core region. It is important to ensure that this region is flush with the base of the core for this distance to be accurate.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the core from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td>The name of the core.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td>Core</td>
            </tr>
            <tr>
                <td><code>owner</code></td>
                <td>Team the core belongs to.</td>
                <td><a href="{{ site.baseurl }}/facets/teams">Team</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>materials</code></td>
                <td>The materials that make up the core.</td>
                <td><span class="badge badge-primary">Multi Material Matcher</span></td>
                <td>obsidian</td>
            </tr>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region containing the core.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>leak</code></td>
                <td>Distance in blocks below the bottom of the core region that the lava must leak.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>5</td>
            </tr>
            <tr>
                <td><code>first-mode</code></td>
                <td><a href="#mode-changes" class="table-ref-link fas fa-chevron-down"></a> The first mode that the core will transition to.</td>
                <td><a href="#mode-changes">Mode</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>destroyable</code></td>
                <td>Whether the core is destroyable by TNT explosions.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>

    #### Examples

    ```xml
    <cores leak="10" name="Core" materials="obsidian">
        <core owner="blue-team" region="blue-core"/>
        <core owner="red-team" region="red-core"/>
    </cores>
    ```
    ### Destroy the Monument

    Destroy the Monument is a game where teams must break a specific monument structure in enemy territory either completely or partially. Destroy the Monument uses the `<game>dtm</game>` game tag.

    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<monuments>' | escape}}</code></td>
                <td>Element containing all monument objectives.</td>
                <td><span class="badge badge-secondary">Monuments Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<monument>' | escape}}</code></td>
                <td>A single monument objective.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<monument>' | escape}}</code> Attributes

In order to reference a monument objective in other facets, such as [Filters]({{ site.baseurl }}/facets/filters), the monument must have a unique ID name. A monument is traditionally composed of obsidian, emerald, or gold, but could be any combination of materials.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the monument from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td>The name of the monument.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td>Monument</td>
            </tr>
            <tr>
                <td><code>owner</code></td>
                <td>Team the monument belongs to.</td>
                <td><a href="{{ site.baseurl }}/facets/teams">Team</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>materials</code></td>
                <td>The materials that make up the monument.</td>
                <td><span class="badge badge-primary">Multi Material Matcher</span></td>
                <td>obsidian</td>
            </tr>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region containing the monument.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>completion</code></td>
                <td>Distance below the bottom of the core region that the lava must leak.</td>
                <td><span class="badge badge-primary">Percentage</span></td>
                <td>100%</td>
            </tr>
            <tr>
                <td><code>first-mode</code></td>
                <td><a href="#mode-changes" class="table-ref-link fas fa-chevron-down"></a> The first mode that the monument will transition to.</td>
                <td><a href="#mode-changes">Mode</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>destroyable</code></td>
                <td>Whether the monument is destroyable by TNT explosions.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<monuments materials="obsidian" first-mode="beacon-mode" name="Monument">
    <monument owner="blue-team" region="blue-monument"/>
    <monument owner="red-team" region="red-monument"/>
</monuments>
```

### Mode Changes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<modes>' | escape}}</code></td>
                <td>Element containing all destroyable modes.</td>
                <td><span class="badge badge-secondary">Modes Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<mode>' | escape}}</code></td>
                <td>A single destroyable mode.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<mode>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the mode from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td>The name of the mode.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>delay</code></td>
                <td>The delay after the previous mode or match start before this mode is attempted to be applied.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>countdown-message</code></td>
                <td>The message to display during the mode countdown.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>success-message</code></td>
                <td>The message to display when the mode is applied.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>fail-message</code></td>
                <td>The message to display when the mode fails after all retry attempts have been made.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>filter</code></td>
                <td>Filter to test for each try of applying the mode.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>Always</td>
            </tr>
            <tr>
                <td><code>retry-attempts</code></td>
                <td>The amount of attempts made to try and apply the mode. An attempt will fail if the filter returns deny.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>retry-delay</code></td>
                <td>The time between each retry attempt.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>pass-mode</code></td>
                <td>The next mode to transition to if the filter passes any of the tries.</td>
                <td><a href="#mode-changes">Mode</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>fail-mode</code></td>
                <td>The next mode to transition to if the filter still fails after all retry attempts.</td>
                <td><a href="#mode-changes">Mode</a></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<mode>' | escape}}</code> Sub-elements

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<materials>' | escape}}</code></td>
                <td>Element containing all the material rules for the mode.</td>
                <td><span class="badge badge-secondary">Materials Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<material>' | escape}}</code></td>
                <td>A single material rule.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<material>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>find</code></td>
                <td>The material to find.</td>
                <td><a href="{{ site.baseurl }}/references/inventory-materials">Material Name</a></td>
            </tr>
            <tr>
                <td><code>replace</code></td>
                <td>The material that will replace the material found.</td>
                <td><a href="{{ site.baseurl }}/references/inventory-materials">Material Name</a></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<modes>
    <mode id="beacon-mode" delay="12m" name="Beacon Monument"> <!-- Objective turns into beacon after 12 minutes -->
      <materials>
        <material find="obsidian" replace="beacon"/>
      </materials>
    </mode>
</modes>
```
