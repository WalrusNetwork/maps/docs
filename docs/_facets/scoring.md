---
layout: facets
title: Scoring
category: games
order: 4
---

### Overview

The score facet allows you to have teams or players complete to score the most points in a certain amount of time or to a specified limit. The competitor with the most points, or the one that reaches the limit first, will win the game.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<score>' | escape}}</code></td>
                <td>Element containing all scoring configuration.</td>
                <td><span class="badge badge-secondary">Score Sub-elements</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<score>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>kills</code></td>
                <td>The number of points to award per kill.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>deaths</code></td>
                <td>The number of points to deduct per accidental death.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>limit</code></td>
                <td>The number of points a competitor must reach in order to win the game.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- set a score limit of 250 points -->
<score kills="1" limit="250"/>
```

#### Interactivity

By default, maps with the score facet will set players into adventure mode. This means you do not need to set up any special applicator rules in order to prevent block interactions. However, if you want block interaction in a map with scoring, you must include <code>{{'<interactive>true</interactive>' | escape}}</code> anywhere in the XML, but preferably near the score facet.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="15%">Element Text</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<interactive>' | escape}}</code></td>
                <td>Specifies if a map using the score facet should have block interaction.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

### Score Boxes

Scoreboxes allow players to score extra points for their team by entering a specified region in the map.

<div class="callout callout-info">Portals are not needed to teleport players that scored in a score box back to their spawn. Score boxes automatically teleport the player on their own.</div>

<div class="callout callout-warning">Score boxes can <strong>only</strong> be used in <strong><a href="#deathmatch">Deathmatch</a></strong> maps.</div>

#### <code>{{'<score>' | escape}}</code> Sub-elements

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<box>' | escape}}</code></td>
                <td>A single score box.</td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<box>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>points</code></td>
                <td>The number of points to award for entering the score box.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>1</td>
            </tr>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region where this score box is located.</td>
                <td><a href="#">Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>filter</code></td>
                <td>Filter who can score in this box.</td>
                <td><a href="#">Filter</a></td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>cooldown</code></td>
                <td>The period of time before the player can score in this box again.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td>0s</td>
            </tr>
            <tr>
                <td><code>heal</code></td>
                <td>Specify if the player should be fully healed when they enter this box.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<score>
    <box region="blue-scorebox" points="2" filter="only-blue" heal="true"/>
    <box region="red-scorebox" points="2" filter="only-red" heal="true"/>
</score>
```

### Redeemables

Not yet supported.

### Game Types

Various game types make use of the scoring facet to award points to competitors when they perform certain actions. These game types include:

- [Capture the Flag]({{ site.baseurl }}/facets/flags#capture-the-flag)
- [King of the Flag]({{ site.baseurl }}/facets/flags#king-of-the-flag)
- [King of the Hill]({{ site.baseurl }}/facets/hills#king-of-the-hill)
- [Deathmatch](#deathmatch)

#### Deathmatch

Deathmatch refers to both the _Team Deathmatch_ and _FFA Deathmatch_ games. Deathmatch is a game where competitors fight each other to get the most points through kills, scoreboxes, and redeemables. Deathmatch uses the `<game>tdm</game>` game tag.

```xml
<game>tdm</game>
<score kills="1"/>
<time-limit>7m</time-limit>
```
