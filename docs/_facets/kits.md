---
layout: facets
title: Kits
category: gear
order: 1
---

### Overview

Kits are groups of items and effects that can be given to players in various ways. There are many types of kits, and they can be packaged together in any combination.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<kits>' | escape}}</code></td>
                <td>Element containing all kits.</td>
                <td><span class="badge badge-secondary">Kits Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<kit>' | escape}}</code></td>
                <td>A single kit.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

In order to reference a specific kit in other facets, such as [Spawns]({{ site.baseurl }}/facets/spawns) or [Applicators]({{ site.baseurl }}/facets/applicators), the kit must have a unique ID name. Kits can also inherit the kit elements from another single kit with the parent attribute. This is useful when you have multiple different kits all with the same items, but small differences. You can put all the constant kit elements in a single kit, have each of the unique kit elements in seperate kits and have each of them inherit from the constant kit.

Kits can also be forced onto a player. If a kit has an item set to slot `0` and the player already has an item in that slot, the item will usually find the next available slot. However, with kit forcing enabled, this item will override and delete the item that was previously in the slot.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the kit from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>parent</code></td>
                <td>The parenting kit to merge with this kit.</td>
                <td><a href="{{ site.baseurl }}/facets/kits">Kit</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>force</code></td>
                <td>Specify if the kit should be force applied, overriding any existing items in their slots.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<kits>
    <kit id="global-kit">
        <effect>night vision</effect>
    </kit>
    <kit id="spawn-kit" parent="global-kit">
        <item slot="0" material="stone sword" unbreakable="true"/>
        <helmet material="leather helmet" unbreakable="true" team-color="true"/>
        <effect duration="6">resistance</effect>
    </kit>
</kits>
```

### Items & Armor

Items are the most simple kit element that can be given to a player. Item stacks are used to provide players with weapons such as swords and bows, materials such as blocks, and armor. Item stacks with slots and special armor item stacks will override anything existing in that slot only if the kit is being force applied to the player. Otherwise, they will find the next available slot to occupy.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<item>' | escape}}</code></td>
                <td>A single item stack.</td>
                <td><a href="{{ site.baseurl }}/facets/items-armor">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<helmet>' | escape}}</code></td>
                <td>A single item stack in the helmet armor slot.</td>
                <td><a href="{{ site.baseurl }}/facets/items-armor">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<chestplate>' | escape}}</code></td>
                <td>A single item stack in the chestplate armor slot.</td>
                <td><a href="{{ site.baseurl }}/facets/items-armor">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<leggings>' | escape}}</code></td>
                <td>A single item stack in the leggings armor slot.</td>
                <td><a href="{{ site.baseurl }}/facets/items-armor">Item Stack</a></td>
            </tr>
            <tr>
                <td><code>{{'<boots>' | escape}}</code></td>
                <td>A single item stack in the boots armor slot.</td>
                <td><a href="{{ site.baseurl }}/facets/items-armor">Item Stack</a></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<kit id="spawn-kit">
    <item slot="0" material="stone sword" unbreakable="true"/>
    <item slot="1" material="bow" unbreakable="true"/>
    <item material="golden carrot" amount="64"/>
    <helmet material="leather helmet" unbreakable="true" team-color="true"/>
</kit>
```

#### Inventory Clearing

You can clear a player's entire inventory using either of the following inventory clearing kits. These can be particularly useful when you are resetting a player's kit when they enter a certain area, or if you have items kept on death and want to remove them before applying the spawn kit to the player.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<clear/>' | escape}}</code></td>
                <td>Clear the all of the player's inventory including armor slots.</td>
            </tr>
            <tr>
                <td><code>{{'<clear-items/>' | escape}}</code></td>
                <td>Clear only the player's item slots.</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- clear the player's inventory before giving new items -->
<kit id="knight-kit">
    <clear/>
    <item slot="0" material="iron sword" unbreakable="true"/>
    <chestplate material="iron chestplate" unbreakable="true"/>
</kit>
```

### Effects

The effect kit is used to give the player potion effects. Effects can be of varying durations and intensities.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<effect>' | escape}}</code></td>
                <td></td>
                <td><a href="{{ site.baseurl }}/references/effects">Effect</a></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>amplifier</code></td>
                <td>The strength of the effect.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>1</td>
            </tr>
            <tr>
                <td><code>duration</code></td>
                <td>The duration that the effect will last.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td>oo (infinite)</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<kit id="effect-kit">
    <!-- infinite night vision -->
    <effect>night vision</effect>
    <!-- strength for 30 seconds -->
    <effect duration="30s">strength</effect>
    <!-- speed 2 for 1 minute -->
    <effect duration="1m" amplifier="2">speed</effect>
</kit>
```

### Health & Hunger

The player's health and hunger attributes can be modfied with the following kit elements. Each kit element accepts a <a href="{{ site.baseurl }}/references/attribute-types">Number Action</a> as an <code>action="..."</code> attribute. By default, the kits will set the specific player attribute to the supplied value.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<health>' | escape}}</code></td>
                <td>Modify the player's current health points. Unless the maximum health has been modified, this can be any number between <code>1</code> and <code>20</code>, inclusive.</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<max-health>' | escape}}</code></td>
                <td>Modify the player's maximum health points. A player normally has 20 health points.</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<health-scale>' | escape}}</code></td>
                <td><a href="https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Player.html#getHealthScale--">Figure out what this actually does.</a></td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<food-level>' | escape}}</code></td>
                <td>Modify the player's current food level. Can be any number between <code>1</code> and <code>20</code>, inclusive.</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<saturation>' | escape}}</code></td>
                <td>Modify the player's current <a href="https://minecraft.gamepedia.com/Saturation" target="_blank">saturation</a>.</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>
        </tbody>
    </table>
</div>

### Skin

A player's skin can be modified to be any particular skin texture through the use of the skin kit.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<set-skin/>' | escape}}</code></td>
                <td>Set the player's skin to a custom defined one.</td>
            </tr>
            <tr>
                <td><code>{{'<clear-skin/>' | escape}}</code></td>
                <td>Resets the player's skin completely to the default Steve/Alex textures.</td>
            </tr>
            <tr>
                <td><code>{{'<reset-skin/>' | escape}}</code></td>
                <td>Resets the player's skin to their own skin.</td>
            </tr>
        </tbody>
    </table>
</div>

<div class="h4"><code>{{'<set-skin/>' | escape}}</code> Attributes</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="25%">Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>data</code></td>
                <td>The Base64 String <code>value</code> of the player's skin texture.</td>
                <td><span class="badge badge-primary">Base64 String</span></td>
            </tr>
            <tr>
                <td><code>signature</code></td>
                <td>The Base64 String <code>signature</code> of the player's skin texture.</td>
                <td><span class="badge badge-primary">Base64 String</span></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<kit id="custom-skin">
    <set-skin data="eyJ0aW1lc3RhbXAiOjE1NjA3NzAxNzIyOTgsInByb2ZpbGVJZCI6ImY2OTBhNTkxMzQ4YjQ4MmVhMThkNzc3OWQwYzBhMjhjIiwicHJvZmlsZU5hbWUiOiJtaXRjaGlpaV8iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzk2YTBkMWEzY2QwMWRiNDBlMWEwMWU3MGRkNTg1M2E1OTAyM2MwN2NmZDdmOWUwOTc2NzkxM2YyN2NmMmFiZWQifX19" signature="X14MTwqS9mnn99FXOkIdqa1lqE6Wf9Fvey/p7leCVD3mCGwjhz/T8wzrc4Nd/IY+yTds0bcb1450+Av4+4qJ+E9VX2crvrMmen73OkYk84LF9D7XCmvCaofTm/4f2+URmXtLarQMhKAhe67dy/29nWusihb7BPIOGwSoccwSc4n3XAsW7CzR3VWV9hm3/GwEXxzwMd8O1vAK28rQNzGHBH/70JrQDspKLl9/7TG9P8XeUoCsjJGuQFneybQrfms6xXtboM4OBFEAFeENqebRAihAb24Zfctr/Jl1v8tGHfWOWLJFpc8brzHzN3Sr8fKk+yV5p++ui9mR5oMmIb/XZ4pN/l7wxY7x1XYRBGPwIitzVsCykSN/xHOVY6JJ5OaDUyr8cyoaLsTtKX8odqm8sE7rfdH5h3DEoVuDut5qMSu3fL66Jm/wb90R6u1HPTsi8yaOdx/eLcf5aAGU8PsGA1p1rSWW54I33LpMr7ka5/41H8gKxBoVViZ6S442TNtbe+JV2YDt52w74uvBwy9XE+aSiuEFB4BbwD9LXGRyRebwEdKE3y17Wk3lcLpptACbv5tnvXEpWT15N3H3bj9+ql+pniGHEhdRQ/YW+N4hlo4UrTneP/IJknmwIgbe10Y9Hga9wBE2NkOUvwPiDNpI9vJTAvnChf6IYPIzT3kYMZg="/>
</kit>
<kit id="reset-skin">
    <reset-skin/>
</kit>
```

Enter the player's <a href="https://mcuuid.net" target="_blank">UUID</a> or Username below and click search. This will open a new window with the Player's data in JSON format. Under <code>textures.raw</code> you will find <code>value</code> and <code>signature</code>. These are the two values you will need to copy.

<div id="lookup-error" class="alert alert-danger" style="display:none"></div>

<div class="form-group input-group">
    <input class="form-control" disabled="true" id="lookup-field" placeholder="Please enable JavaScript in your browser." type="search">
    <span class="input-group-btn">
        <button class="btn btn-primary" id="lookup-submit">
            <i class="fas fa-search"></i>
        </button>
    </span>
</div>

<script>
$(document).ready(function() {
    $('#lookup-field').prop('disabled', false);
    $('#lookup-field').attr('placeholder', 'Enter player UUID or Username');
    $('#lookup-submit').click(function() {
        var input = $('#lookup-field').val();
        if (input) {
            window.open("https://api.ashcon.app/mojang/v2/user/" + input);
        }
        return false;
    });
});
</script>

### Experience

Experience kits modify the player's current experience levels and points. The level modifier changes the numerical level displayed ontop of the experience bar, while points modifies experience on a point level. Each kit element accepts a <a href="{{ site.baseurl }}/references/attribute-types">Number Action</a> as an <code>action="..."</code> attribute. By default, the kits will set the experience level and points to the supplied value.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<exp-level>' | escape}}</code></td>
                <td>Modify the amount of experience levels the player has.</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<exp-points>' | escape}}</code></td>
                <td>Modify the amount of experience points the player has.</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr><!-- no real application, but still exists
            <tr>
                <td><code>{{'<exp-total>' | escape}}</code></td>
                <td>Modify the total amount of experience points the player has collected since last death.</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>-->
        </tbody>
    </table>
</div>

### Movement Speed

Movement speed kits change attributes of the player's motion. Each kit element accepts a <a href="{{ site.baseurl }}/references/attribute-types">Number Action</a> as an <code>action="..."</code> attribute. By default, the kits will set the specific player speed attribute to the supplied value.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<walk-speed>' | escape}}</code></td>
                <td>Modifiers the player's walking speed.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<fly-speed>' | escape}}</code></td>
                <td>Modifies the player's flying speed.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
            </tr>
            <tr>
                <td><code>{{'<exhaustion>' | escape}}</code></td>
                <td>Modifies the player's current <a href="https://minecraft.gamepedia.com/Hunger#Mechanics" target="_blank">exhaustion</a> level.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<kit id="speedy-kit">
    <!-- set the walk speed x2 normal speed -->
    <walk-speed>2</walk-speed>
</kit>
```
```xml
<kit id="slow-kit">
    <!-- subtract 0.5 from the player's current speed -->
    <walk-speed action="subtract">0.5</walk-speed>
</kit>
```

### Double Jump

Double Jump allows players to jump a second time in order to boost themselves further in the air.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<double-jump/>' | escape}}</code></td>
                <td>Allows the player the ability to double jump.</td>
            </tr>
        </tbody>
    </table>
</div>

<div class="h4"><code>{{'<double-jump/>' | escape}}</code> Attributes</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>enabled</code></td>
                <td>Specify if the double jump is allowed. Set to false to remove a player's ability to double jump.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>power</code></td>
                <td>The power of the jump. A normal jump has the power of 1.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td>3</td>
            </tr>
            <tr>
                <td><code>recharge-time</code></td>
                <td>The time before ther player can double jump again.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td>2.5s</td>
            </tr>
            <tr>
                <td><code>recharge-before-landing</code></td>
                <td>Specify if the recharge time should begin before the player lands from their previous jump.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<kit id="double-jump">
    <double-jump power="5"/>
</kit>
```

### Personal Effects

Personal effect kits change features of the world only for the player that the specific kit was applied to.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<compass>' | escape}}</code></td>
                <td>Set the player's compass target to a specific location. Does not give the player a compass.</td>
                <td><span class="badge badge-primary">2D Vector</span></td>
            </tr>
            <tr>
                <td><code>{{'<weather>' | escape}}</code></td>
                <td>Set the player's personal weather.</td>
                <td><code>clear</code>, <code>downfall</code></td>
            </tr>
            <tr>
                <td><code>{{'<time>' | escape}}</code></td>
                <td>Change the player's personal <a href="https://minecraft.gamepedia.com/Day-night_cycle#24-hour_Minecraft_day" target="_blank">time</a>. Can be any number between <code>0</code> and <code>24000</code>, inclusive. Accepts a <a href="{{ site.baseurl }}/references/attribute-types">Number Action</a> as an <code>action="..."</code> attribute (defaults to set).</td>
                <td><span class="badge badge-primary">Number</span></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<kit id="compass-kit">
    <item material="compass"/>
    <!-- set compass target to x 125, z -50.5 -->
    <compass>125,-50.5</compass>
</kit>
```
```xml
<kit id="sunset-kit">
    <!-- set the time to sunset -->
    <time>12000</time>
</kit>
```
