---
layout: facets
title: Spawns
category: mechanics
order: 3
---

### Overview

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<spawns>' | escape}}</code></td>
                <td>Element containing all spawns.</td>
                <td><span class="badge badge-secondary">Spawns Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<spawn>' | escape}}</code></td>
                <td>A single competitor spawn.</td>
                <td><span class="badge badge-secondary">Spawn Sub-elements</span></td>
            </tr>
            <tr>
                <td><code>{{'<default>' | escape}}</code></td>
                <td><span class="badge badge-danger">Required</span> Spawn definition for observers.</td>
                <td><span class="badge badge-secondary">Spawn Sub-elements</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<spawn>' | escape}}</code> Attributes

These attributes also apply to the <code>{{'<default>' | escape}}</code> spawn.

<div class="callout callout-info">Consider making the spawn regions <code>0.5</code> blocks higher than ground level to prevent players glitching in the ground when they spawn.</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>team</code></td>
                <td>The team that will spawn at this spawn. If using the <a href="{{ site.baseurl }}/facets/players">Players</a> format, do not specify a team.</td>
                <td><a href="{{ site.baseurl }}/facets/teams">Team</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>kit</code></td>
                <td>The kit to apply to players when they spawn.</td>
                <td><a href="{{ site.baseurl }}/facets/kits">Kit</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>filter</code></td>
                <td>Filter when players can spawn at this spawn.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>check-air</code></td>
                <td>Check that there are less than 10 blocks of air below where a player will spawn. Spawning will be prevented if no location can be found.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<spawn>' | escape}}</code> Sub-elements

These sub-elements also apply to the <code>{{'<default>' | escape}}</code> spawn.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<region>' | escape}}</code></td>
                <td>Region wrapper containing the regions to spawn in.</td>
                <td><a href="{{ site.baseurl }}/references/regions">Bounded Region</a></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<region>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>yaw</code></td>
                <td>The direction (left,right) the player will be looking when they spawn.</td>
                <td><span class="badge badge-primary">Number</span> / <span class="badge badge-primary">Direction</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>pitch</code></td>
                <td>The direction (up,down) the player will be looking when they spawn.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>look</code></td>
                <td>The location the player will be looking at when they spawn.</td>
                <td><span class="badge badge-primary">3D Vectior</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<spawns>
    <default>
        <region yaw="0">
            <block>-212.5,5,-152.5</block>
        </region>
    </default>
    <spawn team="blue" kit="spawn-kit">
        <region yaw="-90">
            <point>-258.5,7.5,-116.5</point>
        </region>
    </spawn>
    <spawn team="red" kit="spawn-kit">
        <region yaw="90">
            <point>-166.5,7.5,-116.5</point>
        </region>
    </spawn>
</spawns>
```

### Respawning

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<respawns>' | escape}}</code></td>
                <td>Element containing all respawn settings.</td>
                <td><span class="badge badge-secondary">Repawn Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<respawn/>' | escape}}</code></td>
                <td>A single respawn rule.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<respawn/>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>delay</code></td>
                <td>The delay until the player respawns.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td>2s</td>
            </tr>
            <tr>
                <td><code>auto</code></td>
                <td>Specify if the player automatically respawns after the delay duration has elapsed.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>blackout</code></td>
                <td>Specify if dead players are blinded.</td>
                <td><span class="badge badge-primary">boolean</span></td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>spectate</code></td>
                <td>Specify if dead players can fly around.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>filter</code></td>
                <td>Filter to determine when this respawn rule is used.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>message</code></td>
                <td>The message to display on the respawn screen when they are unable to respawn.</td>
                <td><span class="badge badge-primary">Rich String</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml

```
