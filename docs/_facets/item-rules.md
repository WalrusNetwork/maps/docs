---
layout: facets
title: Item Rules
category: gear
order: 5
---

### Overview

Item rules specify what happens to certain items when they are going to be either dropped or picked up. Through this facet, we can destroy certain materials when they are dropped, define which items should be kept by the player when they die, or specify which tools should be automatically repaired when the same item is picked up.

<div class="callout callout-info">Items will only be repaired if the player is already carrying an item of the same kind. Items will not be merged if they have different enchantments or attribute modifiers.</div>

Repair, remove, and keep each accept a filter as their child elements. This filter will typically be a lot of <code>{{'<material>...</material>' | escape}}</code> filters wrapped in a single <code>{{'<any>...</any>' | escape}}</code> filter modifier.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<items>' | escape}}</code></td>
                <td>Element containing each individual repair, remove, and keep rules.</td>
                <td><span class="badge badge-secondary">Items Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<repair>' | escape}}</code></td>
                <td>The repair element, containing a filter for which materials to repair when picked up.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
            </tr>
            <tr>
                <td><code>{{'<remove>' | escape}}</code></td>
                <td>The remove element, containing a filter for which materials to remove when dropped by a player or block.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
            </tr>
            <tr>
                <td><code>{{'<keep>' | escape}}</code></td>
                <td>The keep element, containing a filter for which materials to keep on death.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<items>
    <!-- repair any of the following items when the player picks up another of the same kind -->
    <repair>
        <any>
            <material>stone sword</material>
            <material>bow</material>
            <material>iron axe</material>
            <material>iron pickaxe</material>
        </any>
    </repair>
    <!-- remove any of the following items when they are dropped -->
    <remove>
        <any>
            <material>log</material>
            <material>wood</material>
        </any>
    </remove>
</items>
```
