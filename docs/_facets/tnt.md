---
layout: facets
title: TNT
category: mechanics
order: 6
---

### Overview

You can modify TNT explosion & placing behavior with this module. Destruction of terrain can be easily disabled with the `<blockdamage>` element. If it is set to false, the yield element will have no effect since there won’t be any block drops. TNT damage can be further limited with a filter to disable damage.

By default, dispensers containing TNT will ignite their TNT when exploded. This behavior can be customized with the `<dispenser-tnt-limit>` and `<dispenser-tnt-multiplier>` elements; setting either of these to 0 will disable it.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<tnt>' | escape}}</code></td>
                <td>Node containing the custom TNT settings.</td>
                <td><span class="badge badge-secondary">TNT Sub-elements</span></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">TNT Sub-elements</th>
                <th width="55%">Description</th>
                <th width="15%">Element Text</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<instantignite>' | escape}}</code></td>
                <td>Instant igniting of TNT when placed.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td width="15%">false</td>
            </tr>
            <tr>
                <td><code>{{'<blockdamage>' | escape}}</code></td>
                <td>Block damage of TNT.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td width="15%">false</td>
            </tr>
            <tr>
                <td><code>{{'<yield>' | escape}}</code></td>
                <td>Yield of TNT.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<power>' | escape}}</code></td>
                <td>Power of TNT.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<fuse>' | escape}}</code></td>
                <td>Fuse duration of TNT.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<dispenser-tnt-limit>' | escape}}</code></td>
                <td>Limit of dispenser explosions.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<dispenser-tnt-multiplier>' | escape}}</code></td>
                <td>Multiplier of dispenser explosions.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Example

```xml
<tnt>
    <instantignite>on</instantignite>
    <fuse>2s</fuse>
    <dispenser-tnt-limit>8</dispenser-tnt-limit>
    <dispenser-tnt-multiplier>1</dispenser-tnt-multiplier>
</tnt>
```
