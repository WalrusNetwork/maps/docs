---
layout: facets
title: Applicators
category: mechanics
order: 2
---

### Overview

Applicators combine both the [filters]({{ site.baseurl }}/facets/filters) and [regions]({{ site.baseurl }}/facets/regions) facets to filter what specific events are allowed to occur in a specified region. This is useful for restricting certain teams from entering another team's base, or stopping from certain blocks being destroyed. The order in which applicators are defined are very important. Applicators are parsed top down and those at the top take precedence over those defined below them. For a guide to understanding the order of applicators, please read [this guide]({{ site.baseurl }}/guides/order-of-applicators).

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<applicators>' | escape}}</code></td>
                <td>Element containing all region applicators.</td>
                <td><span class="badge badge-secondary">Applicator Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<apply/>' | escape}}</code></td>
                <td>A single region applicator rule.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<apply/>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> The region to apply the filters to.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Region</a></td>
                <td>everywhere</td>
            </tr>
            <tr>
                <td><code>enter</code></td>
                <td>Filter to determine which players can enter the region.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>abstain</td>
            </tr>
            <tr>
                <td><code>leave</code></td>
                <td>Filter to determine which players can exit the region.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>abstain</td>
            </tr>
            <tr>
                <td><code>block</code></td>
                <td>Filter to determine which blocks can be placed and broken within the region.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>abstain</td>
            </tr>
            <tr>
                <td><code>block-place</code></td>
                <td>Filter to determine which blocks can be placed within the region.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>abstain</td>
            </tr>
            <tr>
                <td><code>block-break</code></td>
                <td>Filter to determine which blocks can be broken within the region.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>abstain</td>
            </tr>
            <tr>
                <td><code>use</code></td>
                <td>Filter to determine which right-click events, such as opening a chest, are allowed within the region.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>abstain</td>
            </tr>
            <tr>
                <td><code>message</code></td>
                <td>The message to display when an action is denied.</td>
                <td><span class="badge badge-primary">Translation String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>velocity</code></td>
                <td>The velocity to apply to the player when they enter the region.</td>
                <td><span class="badge badge-primary">3D Vector</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>kit</code></td>
                <td>The kit to apply to the player when they enter the region.</td>
                <td><a href="{{ site.baseurl }}/facets/kits">Kit</a></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<applicators>
    <!-- deny all block events in the 'team-bases' region -->
    <apply region="team-bases" block="never" message="{region.error.modify.spawn}"/>
    <!-- only allow players that match the 'only-red' filter to enter 'red-base' -->
    <apply region="red-base" enter="only-red" message="{region.error.enter.enemy-spawn}"/>
    <!-- apply an upwards velocity to players when they enter the 'jump' region -->
    <apply region="jump" velocity="0,1.5,0"/>
</applicators>
```
