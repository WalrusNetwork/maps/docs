---
layout: facets
title: Broadcasts
category: information
order: 0
---

### Overview

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<broadcasts>' | escape}}</code></td>
                <td>Element containing all tip and alert broadcasts.</td>
                <td><span class="badge badge-secondary">Broadcasts Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Sub-elements</strong></td>
                <td><strong>Element Text</strong></td>
            </tr>
            <tr>
                <td><code>{{'<tip>' | escape}}</code></td>
                <td>A single [Tip] message.</td>
                <td><span class="badge badge-primary">Translation String</span></td>
            </tr>
            <tr>
                <td><code>{{'<alert>' | escape}}</code></td>
                <td>A single [Alert] message.</td>
                <td><span class="badge badge-primary">Translation String</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>after</code></td>
                <td>The amount of time to wait after the match begings to show the message.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>every</code></td>
                <td>The amount of time to wait to repeat the message.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>count</code></td>
                <td>The amount of times the message is repeated.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>filter</code></td>
                <td>Specify when and to who the broadcast is shown to after the duration has passed.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<broadcasts>
    <tip after="30s">A [Tip] displayed 30 seconds into the match</tip>
    <alert after="5m">An [Alert] displayed after 5 minutes</alert>
    <tip after="10m" count="3">Repeated [Tip] at 10m, 20m, and 30m</tip>
    <tip after="99s" count="oo">Repeated every 99 seconds, forever</tip>
    <tip after="20m" every="1m">Displayed at 20m and every minute after that</tip>
    <tip after="30m" every="10m" count="3">At 30m, 40m, and 50m</tip>
</broadcasts>
```
