---
layout: facets
title: Renewables
category: mechanics
order: 7
---

### Overview

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<renewables>' | escape}}</code></td>
                <td>Element containing all renewable block rules.</td>
                <td><span class="badge badge-secondary">Renewables Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<renewable>' | escape}}</code></td>
                <td>An individual renewable block rule.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<renewable>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>region</code></td>
                <td><span class="badge badge-danger">Required</span> Region in which blocks will be renewed.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>renew-filter</code></td>
                <td>Filter which blocks are renewed.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>replace-filter</code></td>
                <td>Filter which blocks can be replaced by renewing blocks.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>rate</code></td>
                <td>Approximate number of blocks that will be restored per second. Cannot be combined with <code>interval</code>.</td>
                <td><span class="badge badge-primary">Deciaml Number</span></td>
                <td>1</td>
            </tr>
            <tr>
                <td><code>interval</code></td>
                <td>Average time required for an individual block to renew. Cannot be combined with <code>rate</code>.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>grow</code></td>
                <td>Only allow blocks to be restored adjacent to other blocks that are already renewed, or not renewable. If set to false, blocks will be restored at completely random locations, even in mid-air.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>natural</code></td>
                <td>Show block restore particle effects and play block restore sound effects.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
            <tr>
                <td><code>avoid-players</code></td>
                <td>Prevents blocks from being restored within the specified distance of any player.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td>2</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml

```
