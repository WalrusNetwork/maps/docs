---
layout: facets
title: Spawners
category: mechanics
order: 9
---

### Overview

Spawners allows you to create virtual spawners within the level without the need for a spawner block. This allows you to have better control over when and where an entity is spawned without messing around with NBT data. All spawners must be created through the spawner facet instead of the spawner block.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<spawners>' | escape}}</code></td>
                <td>Element containing all virtual spawners.</td>
                <td><span class="badge badge-secondary">Spawners Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<spawner>' | escape}}</code></td>
                <td>An individual virtual spawner.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<spawner>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>spawn-region</code></td>
                <td><span class="badge badge-danger">Required</span> Region in which the spawner will spawn entities in.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>detect-region</code></td>
                <td><span class="badge badge-danger">Required</span> Region in which the spawner will detect players in. Players must be detected for the spawn delay to count down.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>spawn-filter</code></td>
                <td>Filter when the spawner is allowed to spawn entities.</td>
                <td><a href="{{ site.baseurl }}/facets/filters">Filter</a></td>
                <td>always</td>
            </tr>
            <tr>
                <td><code>delay</code></td>
                <td>Time before the spawner spawns when players are within the detection region.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td>200</td>
            </tr>
            <tr>
                <td><code>amount</code></td>
                <td>The number of times the child entity will be spawned in the spawn region each spawn.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>1</td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<spawner>' | escape}}</code> Sub-elements

##### Spawnables

###### <code>{{'<item>' | escape}}</code>

Spawns the specified item stack within the spawn region. **Refer to [Items &amp; Armor]({{ site.baseurl }}/facets/items-armor) for syntax and examples.**

```xml
<item material="golden apple"/>
```

###### <code>{{'<effect>' | escape}}</code>

Spawns the specified potion effect within the spawn region as a thrown splash potion. **Refer to [Kits / Effects]({{ site.baseurl }}/facets/kits#effects) for syntax and examples.**

```xml
<effect>regeneration</effect>
```

###### <code>{{'<entity>' | escape}}</code>

Spawns the specified [entitiy]({{ site.baseurl}}/references/entity-types) within the spawn region.

```xml
<entity>sheep</entity>
```

###### <code>{{'<tnt>' | escape}}</code>

Spawns a Primed TNT entity within the spawn region.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>power</code></td>
                <td>Power of TNT.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>fuse</code></td>
                <td>Fuse duration of TNT.</td>
                <td><span class="badge badge-primary">Decimal Number</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<tnt power="4" fuse="20"/>
```

##### Logic

###### <code>{{'<all>' | escape}}</code>

The all logic modifier will result in all of the child spawnables in being spawned as 1 single spawn. This means that if the `amount` of entities that the spawner will spawn is greater than 1, all of the child entities will be spawned multiple times.

```xml
<!-- spawn both the diamond and golden apple -->
<all>
    <item material="diamond"/>
    <item material="golden apple"/>
</all>
```

###### <code>{{'<any>' | escape}}</code>

The any logic modifier will result in one of the child spawnables in being spawned as 1 single spawn.

```xml
<!-- spawn either the diamond or the golden apple -->
<any>
    <item material="diamond"/>
    <item material="golden apple"/>
</any>
```

#### Examples

```xml
<spawners>
    <spawner detect-region="detect-region" spawn-region="spawn-region" delay="8s">
        <any>
            <tnt power="20" fuse="8"/>
            <item amount="16" damage="2" material="wood"/>
            <effect duration="900" amplifier="1">jump</effect>
        </any>
    </spawner>
</spawners>
```

### Standard Spawners

These spawners are standardized configurations and should be used in their respective contexts.

#### Golden Apples

Spawns a single golden apple every **7 seconds**. These spawners must be represented with a **gold pressure plate** within the Minecraft world which is where the item will spawn.

```xml
<spawner detect-region="detect-region" spawn-region="spawn-region" delay="7s">
    <any>
        <item material="golden apple"/>
    </any>
</spawner>
```

#### Arrows

Spawns a single arrow every **1 second**. These spawners must be represented with an **iron pressure plate** within the Minecraft world which is where the item will spawn.

```xml
<spawner detect-region="detect-region" spawn-region="spawn-region" delay="1s">
    <any>
        <item material="arrow"/>
    </any>
</spawner>
```

#### Blocks

Spawns 3 blocks every **5 seconds**. These spawners must be represented with an **stone pressure plate** within the Minecraft world which is where the items will spawn.

```xml
<spawner detect-region="detect-region" spawn-region="spawn-region" delay="5s">
    <any>
        <item amount="3" material="smooth brick"/>
    </any>
</spawner>
```

#### TNT

Spawns 1 tnt block every **15 seconds**. These spawners must be represented with an **stone pressure plate** within the Minecraft world which is where the item will spawn.

```xml
<spawner detect-region="detect-region" spawn-region="spawn-region" delay="15s">
    <any>
        <item material="tnt"/>
    </any>
</spawner>
```
