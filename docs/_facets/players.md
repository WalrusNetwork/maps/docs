---
layout: facets
title: Players
category: format
order: 1
---

### Overview

<div class="callout callout-danger">This format cannot be used in conjunction with the <a href="{{ site.baseurl }}/facets/teams">Teams</a> format.</div>

Players are a type of competitor in a match where every player is own their own team. Also known as _free-for-all_.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<players>' | escape}}</code></td>
                <td>A single players node containing the players team name.</td>
                <td><span class="badge badge-primary">String</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<players>' | escape}}</code> Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>color</code></td>
                <td><span class="badge badge-danger">Required</span> The display color for the players.</td>
                <td><a href="#">Chat Color</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>min</code></td>
                <td>Required amount of players for the match to begin.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>2</td>
            </tr>
            <tr>
                <td><code>max</code></td>
                <td><span class="badge badge-danger">Required</span> Maximum players for allowed to join the match, normal players cannot join the match once it reaches this size.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>max-overfill</code></td>
                <td>Maximum players hard limit for the match, nobody can join the match once this limit is reached. Must be greater than the defined <code>max</code>.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>max*1.05</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<players max="8" color="yellow">Players</players>
```
