---
layout: facets
title: Capture
category: games
order: 0
---

### Overview

Capture refers to the _Capture the Wool_ game. Capture the Wool is a game where teams must retrieve wool from enemy territory and then successfully bring it back to be placed on a victory monument. Capture the Wool uses the `<game>ctw</game>` game tag.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<wools>' | escape}}</code></td>
                <td>Element containing all wool objectives.</td>
                <td><span class="badge badge-secondary">Wools Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<wool>' | escape}}</code></td>
                <td>A single wool objective.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<wool>' | escape}}</code> Attributes

In order to reference a wool objective in other facets, such as [Filters]({{ site.baseurl }}/facets/filters), the wool must have a unique ID name. The wool must have both a source and destination region, which is where the wool can be found and must be placed. The source region must cover the entire wool room, or enough to encompass each of the containers inside of the wool room. This will automatically protect them from being destroyed and will also allow the wool chests to regenerate the wool stored inside them.

<div class="callout callout-warning">All wool rooms must contain an <strong>Ender Chest</strong> inside of the <code>source</code> region, as this will automatically be filled with wool of the defined <code>color</code>.</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference the wool from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>team</code></td>
                <td><span class="badge badge-danger">Required</span> The team which has to place the wool.</td>
                <td><a href="{{ site.baseurl }}/facets/teams">Team</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>color</code></td>
                <td><span class="badge badge-danger">Required</span> The wool's color.</td>
                <td><a href="{{ site.baseurl }}/references/colors">Color</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>source</code></td>
                <td><span class="badge badge-danger">Required</span> Location where the wool can be found at. Must cover the entire wool room.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>destination</code></td>
                <td><span class="badge badge-danger">Required</span> The monument where the wool has to be placed.</td>
                <td><a href="{{ site.baseurl }}/facets/regions">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>craftable</code></td>
                <td>Specify if this wool can be crafted with wool and dye.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<!-- Define where the wool destinations are -->
<regions>
    <cuboid id="cyan-woolroom" min="-17,0,66" max="-37,25,79"/>
    <block id="cyan-monument">-2,10,-81</block>
    <cuboid id="yellow-woolroom" min="17,0,-65" max="37,25,-78"/>
    <block id="yellow-monument">1,10,81</block>
</regions>
<!-- Define the woll objectives -->
<wools>
    <wool team="blue-team" color="cyan" source="cyan-woolroom" destination="cyan-monument"/>
    <wool team="red-team" color="yellow" source="yellow-woolroom" destination="yellow-monument"/>
</wools>
```
