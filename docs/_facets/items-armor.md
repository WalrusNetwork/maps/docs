---
layout: facets
title: Items & Armor
category: gear
order: 0
---

### Overview

Special armor item stacks have predefined tags to make it easier to give them to a player. Attributes and children elements are the exact same as a standard <code>{{'<item>' | escape}}</code> item stack with the exception that no <code>slot</code> is specified.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<item>' | escape}}</code></td>
                <td>A single item stack.</td>
                <td><span class="badge badge-secondary">Item Sub-elements</span></td>
            </tr>
            <tr>
                <td><code>{{'<helmet>' | escape}}</code></td>
                <td>A single item stack in the helmet armor slot.</td>
                <td><span class="badge badge-secondary">Item Sub-elements</span></td>
            </tr>
            <tr>
                <td><code>{{'<chestplate>' | escape}}</code></td>
                <td>A single item stack in the chestplate armor slot.</td>
                <td><span class="badge badge-secondary">Item Sub-elements</span></td>
            </tr>
            <tr>
                <td><code>{{'<leggings>' | escape}}</code></td>
                <td>A single item stack in the leggings armor slot.</td>
                <td><span class="badge badge-secondary">Item Sub-elements</span></td>
            </tr>
            <tr>
                <td><code>{{'<boots>' | escape}}</code></td>
                <td>A single item stack in the boots armor slot.</td>
                <td><span class="badge badge-secondary">Item Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<enchantments>' | escape}}</code></td>
                <td><a href="#enchantments" class="table-ref-link fas fa-chevron-down"></a> Element containing all enchantments for the item.</td>
                <td><code>{{'<enchantment>' | escape}}</code></td>
            </tr>
            <tr>
                <td><code>{{'<stored-enchantments>' | escape}}</code></td>
                <td><a href="#enchantments" class="table-ref-link fas fa-chevron-down"></a> Element containing all stored enchantments for the item.</td>
                <td><code>{{'<stored-enchantment>' | escape}}</code></td>
            </tr>
            <tr>
                <td><code>{{'<attributes>' | escape}}</code></td>
                <td><a href="#attribute-modifiers" class="table-ref-link fas fa-chevron-down"></a> Element containing all attribute modifiers for the item.</td>
                <td><code>{{'<attribute>' | escape}}</code></td>
            </tr>
            <tr>
                <td><code>{{'<lore>' | escape}}</code></td>
                <td><a href="#lore" class="table-ref-link fas fa-chevron-down"></a> Element containing all lines of lore for the item.</td>
                <td><code>{{'<line>' | escape}}</code></td>
            </tr>
            <tr>
                <td><code>{{'<effects>' | escape}}</code></td>
                <td><a href="#potions" class="table-ref-link fas fa-chevron-down"></a> Element containing all potion effects for the item.</td>
                <td><code>{{'<effect>' | escape}}</code></td>
            </tr>
            <tr>
                <td><code>{{'<pattern>' | escape}}</code></td>
                <td><a href="#banners" class="table-ref-link fas fa-chevron-down"></a> A single pattern layer.</td>
                <td><a href="{{ site.baseurl }}/references/patterns">Pattern</a></td>
            </tr>
        </tbody>
    </table>
</div>

### Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>slot</code></td>
                <td>The slot where the item will be placed in the player's inventory. If no slot is specified, the item will be merged into the player's inventory.</td>
                <td><a href="{{ site.baseurl }}/references/inventory-materials">Inventory Slot</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>material</code></td>
                <td><span class="badge badge-danger">Required</span> The material name for the item stack.</td>
                <td><a href="{{ site.baseurl }}/references/inventory-materials">Material Name</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>damage</code></td>
                <td>The damage value for the material. Used to specify material variants.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>0</td>
            </tr>
            <tr>
                <td><code>amount</code></td>
                <td>The amount of items in the stack.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>1</td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td>The custom name for the item.</td>
                <td><span class="badge badge-primary">Rich String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>color</code></td>
                <td>The color to dye leather armor.</td>
                <td><a href="{{ site.baseurl }}/references/colors">Color</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>team-color</code></td>
                <td>Use color variant that matches the player's team if available.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>potion</code></td>
                <td>The base potion effect for the potion item.</td>
                <td><a href="{{ site.baseurl }}/references/effects">Potion Type</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>splash</code></td>
                <td>Specifies if the potion item is a splash variant.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>flags</code></td>
                <td>A semicolon seperated list of <a href="https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/inventory/ItemFlag.html" target="_blank">Bukkit item flags</a> to apply to the material. Available flags include: <code title="Hides any enchantments">hide enchants</code>, <code title="Hides any attributes">hide attributes</code>, <code title="Hides the unbreakable title">hide unbreakable</code>, <code title="Hides what this item can destroy">hide destroy</code>, <code title="Hides what this material can be placed on">hide placed on</code>, and <code title="Hides any potion effects">hide potion effects</code>.</td>
                <td>Flag List</td>
                <td></td>
            </tr>
            <tr>
                <td><code>unbreakable</code></td>
                <td>Specifies if the item is unbreakable.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>locked</code></td>
                <td>Specifies if the item is locked current slot.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
            <tr>
                <td><code>unshareable</code></td>
                <td>Specifies if the item is unable to be shared by moving it to containers or by dropping.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>false</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<item slot="0" material="iron sword" unbreakable="true"/>
<item slot="1" material="wood" damage="1" amount="48"/>
<item slot="2" material="potion" splash="true" potion="strength"/>
<helmet meterial="iron helmet" unbreakable="true" flags="hide unbreakable;hide attributes"/>
<chestplate material="leather chestplate" team-color="true" unbreakable="true" locked="true"/>
```

### Enchantments

Any enchantment can be applied to any item and an item can have one or multiple enchantments. To store an enchantment in an enchanted book (instead of enchanting the book itself), use <code>{{'<stored-enchantment>' | escape}}</code> instead of the regular enchantment. Each enchantment must be wrapped in either <code>{{'<enchantments> ... </enchantments>' | escape}}</code> or <code>{{'<stored-enchantments> ... </stored-enchantments>' | escape}}</code>.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<enchantment>' | escape}}</code></td>
                <td>An item enchantment.</td>
                <td><a href="{{ site.baseurl }}/references/enchantments">Enchantment</a></td>
            </tr>
            <tr>
                <td><code>{{'<stored-enchantment>' | escape}}</code></td>
                <td>An enchantment stored in an enchanted book.</td>
                <td><a href="{{ site.baseurl }}/references/enchantments">Enchantment</a></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>level</code></td>
                <td>The level for the specified enchantment.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>1</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<!-- sharpness 1 iron sword -->
<item slot="0" material="iron sword" unbreakable="true">
    <enchantments>
        <enchantment>sharpness</enchantment>
    </enchantments>
</item>

<!-- protection 2 iron chestplate -->
<chestplate material="iron chestplate" unbreakable="true">
    <enchantments>
        <enchantment level="2">protection</enchantment>
    </enchantments>
</chestplate>

<!-- feather falling enchanted book -->
<item material="enchanted book">
    <stored-enchantments>
        <stored-enchantment>feather falling</stored-enchantment>
    </stored-enchantments>
</item>
```

### Attribute Modifiers

Not yet supported.

### Lore

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<line>' | escape}}</code></td>
                <td>A single line of lore.</td>
                <td><span class="badge badge-primary">Rich String</span></td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<item material="golden hoe">
    <lore>
        <line>^7A powerful relic of an era long ago.</line>
    </lore>
</item>
```

### Potions

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<effect>' | escape}}</code></td>
                <td>A single potion effect.</td>
                <td><a href="{{ site.baseurl }}/references/effects">Effect Type</a></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>amplifier</code></td>
                <td>The strength of the effect.</td>
                <td><span class="badge badge-primary">Number</span></td>
                <td>1</td>
            </tr>
            <tr>
                <td><code>duration</code></td>
                <td>The duration that the effect will last.</td>
                <td><span class="badge badge-primary">Duration</span></td>
                <td>oo (infinite)</td>
            </tr>
        </tbody>
    </table>
</div>

```xml
<item material="potion">
    <effects>
        <effect duration="10s">increase damage</effect>
        <effect duration="10s" amplifier="2">speed</effect>
    </effects>
</item>
```

### Banners

You can apply patterns to Banner items by layering different <code>{{'<pattern>' | escape}}</code>s on top of one another. Patterns are applied to the banner from bottom to top, meaning that the pattern that is defined first in the list will be at the very front of the banner.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<pattern>' | escape}}</code></td>
                <td>A single pattern layer.</td>
                <td><a href="{{ site.baseurl }}/references/patterns">Pattern</a></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>color</code></td>
                <td>The color for the specified layer.</td>
                <td><a href="{{ site.baseurl }}/references/colors">Color</a></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

The following example would produce <a href="https://i.imgur.com/3TIEy3u.png" target="_blank">this banner</a>.

```xml
<item material="banner" damage="1">
    <pattern color="orange">triangles top</pattern>
    <pattern color="yellow">stripe top</pattern>
    <pattern color="orange">creeper</pattern>
    <pattern color="orange">half horizontal</pattern>
    <pattern color="black">rhombus middle</pattern>
</item>
```
