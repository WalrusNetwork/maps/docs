---
layout: facets
title: Game Modifiers
category: games
order: 5
---

### Overview

Game modifiers aren't games on their own, but change the way other games are played.

### Time Limit

A time limit sets the amount of time that a match can last. Once the time has elapsed, the match will end an a result is calculated in order to determine the match winner. The result attribute determines how the winning team or player is calculated when the time runs out. Result can be the ID of a team, or one of the special values; <code>tie</code> for no winner, or <code>objective</code> for the team that has the most objectives completed.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Element Text</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<time-limit>' | escape}}</code></td>
                <td>The amount of time that the match is set to last.</td>
                <td><span class="badge badge-primary">Duration</span></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>result</code></td>
                <td>The game result once the time limit has elapsed. Accepts a Team ID, <code>tie</code>, or <code>objective</code>. (not yet supported)</td>
                <td>Victory Condition</td>
                <td>objective</td>
            </tr>
        </tbody>
    </table>
</div>

### Blitz

Not yet supported.

### Rage

Enable the one hit kill, rage style gamemode. One hit kills will only be applied to items that have a sharpness enchantment greater than level 1. One shot arrow kills will only be applied to bows that have a power enchantment greater than level 1.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<rage/>' | escape}}</code></td>
                <td>Makes everything a one-hit kill.</td>
            </tr>
        </tbody>
    </table>
</div>
