---
layout: facets
title: Required Elements
category: getting-started
order: 1
---

### Overview

Every map XML file must contain the base <code>{{'<map>' | escape}}</code> element. It contains modules that specify the map name, version, objective, authors, contributors and all other map settings. As per our our [XML Conventions]({{ site.baseurl }}/guides/xml-conventions), all main children of the <code>{{'<map>' | escape}}</code> element must be inline with it and not indented. Each subsequent element inside of the main children elements must be indented with 4 spaces relative to the parent element. Never use tabs.

<div class="callout callout-info">New to XML and the documentation? You can find a guide on getting started <a href="{{ site.baseurl }}/guides/getting-started">here</a>.</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<map>' | escape}}</code></td>
                <td>The main map element containing all the modules used by the map.</td>
                <td><span class="badge badge-primary">Map Sub-elements</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<map>' | escape}}</code> Attributes

The <code>proto</code> attribute specifies what version of Game Manager the XML file was created for. If this value is higher than the version of Game Manager that is running, the map won’t load. If it’s lower, the map will load but the XML may be interpreted in an outdated way. Mapmakers should always use the latest supported XML version, and this is required of new maps that are to be added to the Walrus  repository.

<div class="callout callout-info">The current map protocol is <strong>{{site.current_proto}}</strong></div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="25%">Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>proto</code></td>
                <td><span class="badge badge-danger">Required</span> The map's XML protocol version.</td>
                <td>{{site.current_proto}}</td>
            </tr>
        </tbody>
    </table>
</div>

#### <code>{{'<map>' | escape}}</code> Sub-elements

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="15%">Value/Children</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<name>' | escape}}</code></td>
                <td><span class="badge badge-danger">Required</span> The name of the map.</td>
                <td><span class="badge badge-primary">String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<version>' | escape}}</code></td>
                <td><span class="badge badge-danger">Required</span> The version number of the map which uses <a href="https://semver.org">Semantic Versioning</a>.</td>
                <td><span class="badge badge-primary">Version</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<game>' | escape}}</code></td>
                <td><span class="badge badge-danger">Required</span> <a href="#game-types" class="table-ref-link fas fa-chevron-down"></a> The game type(s) of the map.</td>
                <td><a href="#game-types">Game Type</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<objective>' | escape}}</code></td>
                <td><span class="badge badge-danger">Required</span> The objective of the map. What must be done in order to win the game.</td>
                <td><span class="badge badge-primary">Translation String</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<stage>' | escape}}</code></td>
                <td>The development stage of this map. Only maps with <code>production</code> stage are able to be set on public servers.</td>
                <td><code>production</code>, <code>development</code></td>
                <td><code>production</code></td>
            </tr>
            <tr>
                <td><code>{{'<authors>' | escape}}</code></td>
                <td><span class="badge badge-danger">Required</span> <a href="#authors-and-contributors" class="table-ref-link fas fa-chevron-down"></a> The authors of the map.</td>
                <td><span class="badge badge-primary">Authors Sub-elements</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>{{'<contributor>' | escape}}</code></td>
                <td><a href="#authors-and-contributors" class="table-ref-link fas fa-chevron-down"></a> The contributors to the map.</td>
                <td><span class="badge badge-primary">Contributors Sub-elements</span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<map proto="{{site.current_proto}}">
<name>Super Awesome Map</name>
<version>1.0.0</version>
<game>dtm</game>
<objective>Destroy the other team's monument!</objective>
</map>
```

### Game Types

Game Types specify the type of match that the map is. It is required for game types to be defined as different types can have different properties. Maps can have multiple game types, however, some game types are not compatible with others.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">ID</th>
                <th width="30%">Description</th>
                <th width="20%">ID</th>
                <th width="30%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>ctw</code></td>
                <td><a href="{{ site.baseurl }}/facets/capture">Capture the Wool</a></td>
                <td><code>ctf</code></td>
                <td><a href="{{ site.baseurl }}/facets/flags">Capture the Flag</a></td>
            </tr>
            <tr>
                <td><code>dom</code></td>
                <td><a href="{{ site.baseurl }}/facets/hills">Domination</a></td>
                <td><code>{{'koth' | escape}}</code></td>
                <td><a href="{{ site.baseurl }}/facets/hills">King of the Hill</a></td>
            </tr>
            <tr>
                <td><code>dtc</code></td>
                <td><a href="{{ site.baseurl }}/facets/destroy">Destroy the Core</a></td>
                <td><code>dtm</code></td>
                <td><a href="{{ site.baseurl }}/facets/destroy">Destroy the Monument</a></td>
            </tr>
            <tr>
                <td><code>tdm</code></td>
                <td><a href="{{ site.baseurl }}/facets/scoring">Team Deathmatch</a></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<!-- a map with both monument and core objectives -->
<game>dtm</game>
<game>dtc</game>
```

### Authors and Contributors

The authors and contributors elements provide information about who created and helped create the map. There can be multiple authors and contributors to a map. The contribution attribute should be used to specify what their contribution to the map was. A player must be credited by their UUID. A UUID is a unique user identifier that is used to keep track of players even if they change their name. You can check player UUID’s at <a href="https://mcuuid.net" target="_blank">mcuuid.net</a>.

#### Sub-elements

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="80%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<author>' | escape}}</code></td>
                <td>A major author of the map. Child to <code>{{'<authors>' | escape}}</code>.</td>
            </tr>
            <tr>
                <td><code>{{'<contributor>' | escape}}</code></td>
                <td>A contributor to the map. Child to <code>{{'<contributors>' | escape}}</code>.</td>
            </tr>
        </tbody>
    </table>
</div>

#### Attributes

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="25%">Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>uuid</code></td>
                <td><span class="badge badge-danger">Required</span> The UUID used to identify a player.</td>
                <td><a href="https://mcuuid.net" target="_blank">UUID</a></td>
            </tr>
            <tr>
                <td><code>contribution</code></td>
                <td>The contribution this author or contributor made to the map.</td>
                <td><span class="badge badge-primary">String</span></td>
            </tr>
        </tbody>
    </table>
</div>

#### Examples

```xml
<authors>
    <author uuid="f690a591-348b-482e-a18d-7779d0c0a28c"/> <!-- mitchiii_ -->
    <author uuid="a9f11454-beac-4cc3-a138-1780802c8794" contribution="Awesome contributions"/> <!-- Crazy_ -->
</authors>
<contributors>
    <contributor uuid="c2a4c847-653e-482a-b62e-d7f8b64330cb" contribution="Clarification of element usage"/> <!-- ALM -->
</contributors>
```

### Basic Template

Below is a template which includes all of the elements discussed on this page which are required. Fill out all the elements in accordance to your map. In order for the XML to be valid and load onto the server, you still must add the following facets:

- A single format ([Teams]({{ site.baseurl }}/facets/teams) or [Players]({{ site.baseurl }}/facets/players))
- A single game type ([Capture the Wool]({{ site.baseurl }}/facets/capture-the-wool), [Capture the Flag]({{ site.baseurl }}/facets/capture-the-flag), or one of the others)
- [Spawns]({{ site.baseurl }}/facets/spawns)

You may also want to include:

- [Kits]({{ site.baseurl }}/facets/kits), to give players gear when they spawn
- [Kill Rewards]({{ site.baseurl }}/facets/kill-rewards), to reward players each kill
- [Applicators]({{ site.baseurl }}/facets/applicators), to control where and what can be interacted with in the map

You can find more complete templates for each gamemode [here]({{ site.baseurl }}/examples/templates).
