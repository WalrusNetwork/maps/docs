---
layout: references
title: Attribute Types
category: misc
order: 0
---

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="25%">Type</th>
                <th width="55%">Accepted Values</th>
                <th width="20%">Example</th>
            </tr>
        </thead>
        <tbody>
            <tr id="number">
                <td>Number</td>
                <td>A whole number.</td>
                <td><code>5</code></td>
            </tr>
            <tr id="decimal-number">
                <td>Decimal Number</td>
                <td>A number, with or without decimals.</td>
                <td><code>2.5</code></td>
            </tr>
            <tr id="range">
                <td>Range</td>
                <td>Two whole numbers seperated by a hyphen ("-").</td>
                <td><code>1-5</code></td>
            </tr>
            <tr id="decimal-range">
                <td>Decimal Range</td>
                <td>Two numbers, with or without decimals, seperated by a hyphen ("-").</td>
                <td><code>1.25-9.75</code></td>
            </tr>
            <tr id="boolean">
                <td>Boolean</td>
                <td>
                    Enabled: <code>true</code>, <code>yes</code>, <code>allow</code>, <code>on</code>, or<br>
                    Disabled: <code>false</code>, <code>no</code>, <code>deny</code>, <code>off</code>.
                </td>
                <td><code>true</code></td>
            </tr>
            <tr id="duration">
                <td>Duration</td>
                <td>
                    <code>oo</code> for infinite duration, or<br>
                    A single Number in seconds, or<br>
                    Numbers appended with <code title="Days">d</code>, <code title="Hours">h</code>, <code title="Minutes">m</code>, or <code title="Seconds">s</code> for durations in specific periods.
                </td>
                <td><code>5m30s</code> (5 min 30 sec)</td>
            </tr>
            <tr id="color">
                <td><a href="{{ site.baseurl }}/references/colors">Color</a></td>
                <td>
                    A <a href="{{ site.baseurl }}/references/colors">color name</a>, or<br>
                    A HEX color prepended with <code>#</code>.
                </td>
                <td><code>red</code></td>
            </tr>
            <tr id="url">
                <td>URL</td>
                <td>A <a href="https://www.ietf.org/rfc/rfc2396.txt" target="_blank">RFC2396 valid URL.</a></td>
                <td><code>https://google.com</code></td>
            </tr>
            <tr id="version">
                <td>Version</td>
                <td>A version number in the <a href="https://semver.org/" target="_blank">Semantic Version</a> format.</td>
                <td><code>1.2.0</code></td>
            </tr>
            <tr id="unique-id">
                <td>Unique ID</td>
                <td>A unique <a href="https://en.wikipedia.org/wiki/Kebab_case" target="_blank">dash-case</a> String.</td>
                <td><code>blue-monument</code></td>
            </tr>
            <tr id="string">
                <td>String</td>
                <td>A string of characters.</td>
                <td><code>Hello!</code></td>
            </tr>
            <tr id="rich-string">
                <td>Rich String</td>
                <td>A rich string of characters that uses <a href="{{ site.baseurl }}/references/text-formatting">Text Formatting</a></td>
                <td><code>^4Hello, in red!</code></td>
            </tr>
            <tr id="translation-string">
                <td><a href="{{ site.baseurl }}/references/translation-strings">Translation String</a></td>
                <td>A translation key wrapped in <code>{}</code>. If the key has arguments, use commas (",") to pass the additional information.</td>
                <td><code>{ui.wins,Blue}</code></td>
            </tr>
            <tr id="material-matcher">
                <td><a href="{{ site.baseurl }}/references/inventory-materials">Material Matcher</a></td>
                <td>A <a href="{{ site.baseurl }}/references/inventory-materials#material_finder">material name and data value</a> separated by a colon (":"). If no data value is specified, it will match all variations of the material.</td>
                <td>
                    <code>wool:0</code> (Only white wool)<br>
                    <code>wool</code> (All wool types)
                </td>
            </tr>
            <tr id="multi-material-matcher">
                <td><a href="{{ site.baseurl }}/references/inventory-materials">Multi Material Matcher</a></td>
                <td>A list of Material Matchers separated by a semicolon (";").</td>
                <td><code>wool;log:0</code> (All wool and oak logs)</td>
            </tr>
            <tr id="3d-vector">
                <td>3D Vector</td>
                <td>An <code>x,y,z</code> coordinate in the Minecraft world.</td>
                <td><code>10,5.5,-10</code></td>
            </tr>
            <tr id="2d-vector">
                <td>2D Vector</td>
                <td>An <code>x,z</code> coordinate in the Minecraft world.</td>
                <td><code>5,25</code></td>
            </tr>
            <tr id="direction">
                <td>Direction</td>
                <td>A cardinal direction: <code>south</code>, <code>south south west</code>, <code>south west</code>, <code>west south west</code>, <code>west</code>, <code>west north west</code>, <code>north west</code>, <code>north north west</code>, <code>north</code>, <code>north north east</code>, <code>north east</code>, <code>east north east</code>, <code>east</code>, <code>east south east</code>, <code>south east</code>, or <code>south south east</code>.</td>
                <td><code>south</code></td>
            </tr>
            <tr id="number-action">
                <td>Number Action</td>
                <td>
                    <code>none</code> to keep the original number, or<br>
                    <code>set</code> to set the number to the specified value (default), or<br>
                    <code>add</code> to add to the base number, or<br>
                    <code>subtract</code> to subtract from the base number, or<br>
                    <code>multiply</code> to multiply the base number, or<br>
                    <code>divide</code> to divide the base number, or<br>
                    <code>power</code> to raise the base number to a power.
                </td>
                <td><code>set</code></td>
            </tr><!--
            <tr>
                <td>Prepared Number Action</td>
                <td>A Number Action which modified with a pre-defined modifier.</td>
                <td></td>
            </tr>-->
            <tr id="number-comparator">
                <td>Number Comparator</td>
                <td>
                    Compare the base number to the specified number using the following comparator functions: <code>equals</code>, <code>less than</code>, <code>less</code>, <code>less than equal</code>, <code>greater than</code>, <code>greater</code>, <code>greater than equal</code> (default), or <code>mod X</code>.
                </td>
                <td><code>greater than equal</code></td>
            </tr>
        </tbody>
    </table>
</div>
