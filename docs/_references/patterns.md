---
layout: references
title: Patterns
category: gear
order: 4
---

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="25%">Bukkit Name</th>
                <th width="25%">Appearance</th>
                <th width="25%">Bukkit Name</th>
                <th width="25%">Appearance</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>base</code></td>
                <td></td>
                <td><code>border</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/border.png"></td>
            </tr>
            <tr>
                <td><code>bricks</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/bricks.png"></td>
                <td><code>circle middle</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/circle_middle.png"></td>
            </tr>
            <tr>
                <td><code>creeper</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/creeper.png"></td>
                <td><code>cross</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/cross.png"></td>
            </tr>
            <tr>
                <td><code>curly border</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/curly_border.png"></td>
                <td><code>diagonal left</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/diagonal_left.png"></td>
            </tr>
            <tr>
                <td><code>diagonal left mirror</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/diagonal_left_mirror.png"></td>
                <td><code>diagonal right</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/diagonal_right.png"></td>
            </tr>
            <tr>
                <td><code>diagonal right mirror</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/diagonal_right_mirror.png"></td>
                <td><code>flower</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/flower.png"></td>
            </tr>
            <tr>
                <td><code>gradient</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/gradient.png"></td>
                <td><code>gradient up</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/gradient_up.png"></td>
            </tr>
            <tr>
                <td><code>half horizontal</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/half_horizontal.png"></td>
                <td><code>half horizontal mirror</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/half_horizontal_mirror.png"></td>
            </tr>
            <tr>
                <td><code>half vertical</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/half_vertical.png"></td>
                <td><code>half vertical mirror</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/half_vertical_mirror.png"></td>
            </tr>
            <tr>
                <td><code>mojang</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/mojang.png"></td>
                <td><code>rhombus middle</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/rhombus_middle.png"></td>
            </tr>
            <tr>
                <td><code>skull</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/skull.png"></td>
                <td><code>square bottom left</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/square_bottom_left.png"></td>
            </tr>
            <tr>
                <td><code>square bottom right</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/square_bottom_right.png"></td>
                <td><code>square top left</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/square_top_left.png"></td>
            </tr>
            <tr>
                <td><code>square top right</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/square_top_right.png"></td>
                <td><code>straight cross</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/straight_cross.png"></td>
            </tr>
            <tr>
                <td><code>stripe bottom</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_bottom.png"></td>
                <td><code>stripe center</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_center.png"></td>
            </tr>
            <tr>
                <td><code>stripe downleft</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_downleft.png"></td>
                <td><code>stripe downright</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_downright.png"></td>
            </tr>
            <tr>
                <td><code>stripe left</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_left.png"></td>
                <td><code>stripe middle</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_middle.png"></td>
            </tr>
            <tr>
                <td><code>stripe right</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_right.png"></td>
                <td><code>stripe small</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_small.png"></td>
            </tr>
            <tr>
                <td><code>stripe top</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/stripe_top.png"></td>
                <td><code>triangle bottom</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/triangle_bottom.png"></td>
            </tr>
            <tr>
                <td><code>triangle top</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/triangle_top.png"></td>
                <td><code>triangles bottom</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/triangles_bottom.png"></td>
            </tr>
            <tr>
                <td><code>triangles top</code></td>
                <td><img src="{{ site.baseurl }}/assets/img/patterns/triangles_top.png"></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>
