---
layout: references
title: Colors
category: misc
order: 3
---

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="15%">Chat Color</th>
                <th width="15%">Block Color</th>
                <th width="15%">Armor Color</th>
                <th width="55%">Firework Color</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>white</td>
                <td>white</td>
                <td><div class="color-square" style="background-color:#ffffff"></div> <code class="color-detail">#ffffff</code></td>
                <td><div class="color-square" style="background-color:#f0f0f0"></div> <code class="color-detail">#f0f0f0</code></td>
            </tr>
            <tr>
                <td>orange</td>
                <td>orange</td>
                <td><div class="color-square" style="background-color:#d87f33"></div> <code class="color-detail">#d87f33</code></td>
                <td><div class="color-square" style="background-color:#eb8844"></div> <code class="color-detail">#eb8844</code></td>
            </tr>
            <tr>
                <td></td>
                <td>magenta</td>
                <td><div class="color-square" style="background-color:#b24cd8"></div> <code class="color-detail">#b24cd8</code></td>
                <td><div class="color-square" style="background-color:#c354cd"></div> <code class="color-detail">#c354cd</code></td>
            </tr>
            <tr>
                <td>aqua</td>
                <td>light blue</td>
                <td><div class="color-square" style="background-color:#6699d8"></div> <code class="color-detail">#6699d8</code></td>
                <td><div class="color-square" style="background-color:#6689d3"></div> <code class="color-detail">#6689d3</code></td>
            </tr>
            <tr>
                <td>yellow</td>
                <td>yellow</td>
                <td><div class="color-square" style="background-color:#e5e533"></div> <code class="color-detail">#e5e533</code></td>
                <td><div class="color-square" style="background-color:#decf2a"></div> <code class="color-detail">#decf2a</code></td>
            </tr>
            <tr>
                <td>lime</td>
                <td>lime</td>
                <td><div class="color-square" style="background-color:#7fcc19"></div> <code class="color-detail">#7fcc19</code></td>
                <td><div class="color-square" style="background-color:#41cd34"></div> <code class="color-detail">#41cd34</code></td>
            </tr>
            <tr>
                <td>pink</td>
                <td>pink</td>
                <td><div class="color-square" style="background-color:#f27fa5"></div> <code class="color-detail">#f27fa5</code></td>
                <td><div class="color-square" style="background-color:#d88198"></div> <code class="color-detail">#d88198</code></td>
            </tr>
            <tr>
                <td>gray</td>
                <td>silver</td>
                <td><div class="color-square" style="background-color:#999999"></div> <code class="color-detail">#999999</code></td>
                <td><div class="color-square" style="background-color:#ababab"></div> <code class="color-detail">#ababab</code></td>
            </tr>
            <tr>
                <td>cyan</td>
                <td>cyan</td>
                <td><div class="color-square" style="background-color:#4c7f99"></div> <code class="color-detail">#4c7f99</code></td>
                <td><div class="color-square" style="background-color:#287697"></div> <code class="color-detail">#287697</code></td>
            </tr>
            <tr>
                <td>purple</td>
                <td>purple</td>
                <td><div class="color-square" style="background-color:#7f3fb2"></div> <code class="color-detail">#7f3fb2</code></td>
                <td><div class="color-square" style="background-color:#7b2fbe"></div> <code class="color-detail">#7b2fbe</code></td>
            </tr>
            <tr>
                <td>blue</td>
                <td>blue</td>
                <td><div class="color-square" style="background-color:#334cb2"></div> <code class="color-detail">#334cb2</code></td>
                <td><div class="color-square" style="background-color:#253192"></div> <code class="color-detail">#253192</code></td>
            </tr>
            <tr>
                <td>dark blue*</td>
                <td>blue</td>
                <td><div class="color-square" style="background-color:#334cb2"></div> <code class="color-detail">#334cb2</code></td>
                <td><div class="color-square" style="background-color:#253192"></div> <code class="color-detail">#253192</code></td>
            </tr>
            <tr>
                <td></td>
                <td>brown</td>
                <td><div class="color-square" style="background-color:#664c33"></div> <code class="color-detail">#664c33</code></td>
                <td><div class="color-square" style="background-color:#51301a"></div> <code class="color-detail">#51301a</code></td>
            </tr>
            <tr>
                <td>green</td>
                <td>green</td>
                <td><div class="color-square" style="background-color:#667f33"></div> <code class="color-detail">#667f33</code></td>
                <td><div class="color-square" style="background-color:#3b511a"></div> <code class="color-detail">#3b511a</code></td>
            </tr>
            <tr>
                <td>red</td>
                <td>red</td>
                <td><div class="color-square" style="background-color:#993333"></div> <code class="color-detail">#993333</code></td>
                <td><div class="color-square" style="background-color:#b3312c"></div> <code class="color-detail">#b3312c</code></td>
            </tr>
            <tr>
                <td>light red*</td>
                <td>red</td>
                <td><div class="color-square" style="background-color:#993333"></div> <code class="color-detail">#993333</code></td>
                <td><div class="color-square" style="background-color:#b3312c"></div> <code class="color-detail">#b3312c</code></td>
            </tr>
            <tr>
                <td>black</td>
                <td>black</td>
                <td><div class="color-square" style="background-color:#161616"></div> <code class="color-detail">#161616</code></td>
                <td><div class="color-square" style="background-color:#1e1b1b"></div> <code class="color-detail">#1e1b1b</code></td>
            </tr>
        </tbody>
    </table>
</div>

<small><sup>*</sup> There isn't any corresponding block or hex colors for this chat color. Teams that use this chat color will recieve blocks of a similar matching color, however another chat color may already use it.</small>
