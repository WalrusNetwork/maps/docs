---
layout: references
title: Translations
category: misc
order: 1
---

### Overview

All messages and strings displayed to a player should be translated in to the player's local language. In order to do this, we use predefined translation keys which are used in the XML in substitution of on English-based string. Some strings take additional arguments which are represented by `{0}` in the translation string. These arguments can be provided after the translation key seperated by a comma for each additional argument.

```xml
<!-- Destroy 75% of the monument in enemy territory! -->
<objective>{map.objective.destroy.monument.singular,75}</objective>
<teams>
    <team color="red" max="4" max-overfill="5">{map.team.red}</team>
</teams>
<applicators>
    <apply region="spawns" block="never" message="{region.error.modify.spawn}"/>
    <apply region="void-areas" block="no-void" message="{region.error.modify.void}"/>
</applicators>
```

### Key Finder

_Tip: click on the translation keys to save them to your clipboard_

<div id="blocks">
    <form role="search">
        <div class="form-group input-group">
            <input class="form-control" disabled="true" id="searchField" oninput="doTranslationSearch('dataTable')" placeholder="No Search - JavaScript Disabled :(" type="search" />
            <div class="input-group-append">
                <span class="input-group-text" id="resultCount"></span>
            </div>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table table-sm table-striped">
            <thead>
                <tr>
                    <th width="50%">Key</th>
                    <th width="50%">String</th>
                </tr>
            </thead>
            <tbody id="dataTable">
            </tbody>
        </table>
    </div>
</div>
<div style="opacity:0">
    <input id="copy" type="text"/>
</div>

Can't find the translation you're looking for? You can request additional translation keys for your requirements [here](#).

<script>
    $(document).ready(function() {
        // Remove the js missing error from the search field.
        document.getElementById('searchField').disabled = false;
        document.getElementById('searchField').placeholder = "Search...";

        // Call this so that the table gets populated and then indexed.
        importTranslations('dataTable', 'en_US');
    });

    // Search a specific table and remove all rows that do not match.
    var parsedRowData = null;

    function doTranslationSearch(tableID) {
        var targetTable = document.getElementById(tableID);

        // Index the table.
        if (!parsedRowData) {
            var rowData = targetTable.rows;
            parsedRowData = new Array();
            for (var rowIndex = 0; rowIndex < rowData.length; rowIndex++) {
                var tableRow = rowData.item(rowIndex);
                // Not a data row, ignore.
                if (tableRow.cells.length != 2) {
                    continue;
                }

                // Get and nicely format the search-able terms in the table.
                // We add the text to an empty string so that we don't do indexOf against null later on in the code.
                var translationKey = '' + tableRow.cells.item(0).textContent.trim().replace(/\s\s+/g, ' ').replace(/(_)/gm, " ").toLocaleLowerCase();
                var translationString = '' + tableRow.cells.item(1).textContent.trim().replace(/\s\s+/g, ' ').toLocaleLowerCase();
                console.log(translationKey)
                parsedRowData.push([tableRow, translationKey, translationString]);
            }
        }

        // Format the search query
        var searchText = document.getElementById('searchField').value.trim().toLocaleLowerCase();
        searchText = searchText.replace(/~\S*/gi, "").trim();

        // Create the query regex pattern
        var searchKeys = searchText.split(' ');
        var searchRegexS = "^";
        for (var i = 0; i < searchKeys.length; i++) {
            searchRegexS += "(?=.*\\b" + searchKeys[i] + ")";
        }
        searchRegexS += ".+";
        searchRegex = new RegExp(searchRegexS, 'i');

        var matchingRows = new Array();
        // Loop through the tables rows and check if they match the query.
        for (var rowIndex = 0; rowIndex < parsedRowData.length; rowIndex++) {
            var rowData = parsedRowData[rowIndex];

            var translationKey = rowData[1];
            var translationString = rowData[2];

            var matches = false;
            // If the search term is probably a translation key.
            substrings = ["{", "}", "\."];
            if (new RegExp(substrings.join("|")).test(searchText)) {
                // Only return true if the search result starts at the beginning of the UID
                // This will prevent a search query of 8 from returning 18.
                if (translationKey.search(searchRegex) != -1) {
                    matches = true;
                }
            }

            // Otherwise match the translation string
            // If searchText is "" (nothing) returns 0 instead of -1?
            // This isn't really a problem since we want the table to show everything if there is no search term.
            // This also allows us to search just by tag without having to add another else if.
            else if (translationString.search(searchRegex) != -1) {
                matches = true;
            }

            // Add matching row
            if (matches == true) {
                matchingRows.push(rowData)
            }
        }

        // Update the result counter.
        var count = matchingRows.length.toString();
        document.getElementById("resultCount").textContent = count + " Results";

        // Create a new table body and replace the old one.
        var newTableBody = document.createElement('tbody');
        newTableBody.setAttribute("id", "dataTable");
        populateWithRows(matchingRows, newTableBody);
        targetTable.parentNode.replaceChild(newTableBody, targetTable)
        return true;
    }

    function populateWithRows(matchingRows, newTableBody) {
        // If there are no matching rows add one indicating this.
        if (matchingRows.length <= 0) {
            var row = document.createElement("tr");
            var cell = document.createElement("td");
            var cellText = document.createTextNode("No translations match the query.");

            cell.appendChild(cellText);
            row.appendChild(cell);
            row.setAttribute("id", "no_items");
            cell.setAttribute("colspan", "4");
            // Insert it as the first child.
            newTableBody.insertBefore(row, newTableBody.firstChild);
        } else {
            for (var rowIndex = 0; rowIndex < matchingRows.length; rowIndex++) {
                matchingRow = matchingRows[rowIndex];
                row = matchingRow[0];
                newTableBody.appendChild(row);
            }
        }
    }
    
    function importTranslations(tableID = "dataTable", lang = "en_US") {
        var targetTable = document.getElementById(tableID);
        var tableBody = document.createElement('tbody');
        var reqFile = "https://gitlab.com/api/v4/projects/14583324/repository/files/" + lang + "%2Fgames%2Foctc%2Fmaps.properties/raw?ref=master";
        $.get(reqFile, function(data) {
            // Array-ify translations and remove any empty lines
            translations = data.split("\n").filter(function (el) {
                return el != "";
            });
            for (var i = 0; i < translations.length; i++) {
                if (translations[i].startsWith("#") == true) {
                    translations[i] = translations[i].replace('# ', '');
                } else {
                    // Split the key from the string
                    translations[i] = translations[i].split("=");
                    // Apply brackets to the key
                    translations[i][0] = "{" + translations[i][0] + "}";
                }
            }
            // Populate table with key string pairs
            for (var i = 0; i < translations.length; i++) {
                // Create the rows and cels
                var row = targetTable.insertRow(targetTable.rows.length);
                var string = row.insertCell(0);
                var key = row.insertCell(0);
                // Insert data into the cells
                if (!Array.isArray(translations[i])) {
                    string.innerHTML = "";
                    key.innerHTML = "<strong>" + translations[i] + "</strong>";
                } else {
                    string.innerHTML = translations[i][1];
                    key.innerHTML = "<code class=\"translation-key\" data-toggle=\"tooltip\" data-placement=\"top\">" + translations[i][0] + "</code>";
                }
            }
            // Copy the key to clipboard when clicked
            $(".translation-key").click(function() {
                var copy = document.getElementById("copy");
                copy.value = this.innerHTML;
                copy.select();
                try {
                    document.execCommand("copy");
                    this.setAttribute("title", "Copied!");
                    $(this).tooltip("enable");
                    $(this).tooltip("show");
                    this.setAttribute("title", "");
                    $(this).tooltip("disable");
                } catch (ex) {
                    console.log(ex);
                }
                copy.value = "";
            });
            doTranslationSearch(tableID);
        });
    }
</script>
