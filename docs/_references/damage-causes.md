---
layout: references
title: Damage Causes
category: entities
order: 2
---

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="25%">Damage</th>
                <th width="75%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>block explosion</code></td>
                <td>Damage caused by being in the area when a block explodes.</td>
            </tr>
            <tr>
                <td><code>contact</code></td>
                <td>Damage caused when an entity contacts a block such as a cactus.</td>
            </tr>
            <tr>
                <td><code>drowning</code></td>
                <td>Damage caused by running out of air while in water.</td>
            </tr>
            <tr>
                <td><code>entity attack</code></td>
                <td>Damage caused when an entity attacks another entity.</td>
            </tr>
            <tr>
                <td><code>entity explosion</code></td>
                <td>Damage caused by being in the area when an entity, such as a creeper, explodes.</td>
            </tr>
            <tr>
                <td><code>fall</code></td>
                <td>Damage caused when an entity falls a distance greater than three blocks.</td>
            </tr>
            <tr>
                <td><code>falling block</code></td>
                <td>Damage caused by being hit by a falling block which deals damage.</td>
            </tr>
            <tr>
                <td><code>fire</code></td>
                <td>Damage caused by direct exposure to fire.</td>
            </tr>
            <tr>
                <td><code>fire tick</code></td>
                <td>Damage caused due to burns caused by fire.</td>
            </tr>
            <tr>
                <td><code>lava</code></td>
                <td>Damage caused by direct exposure to lava.</td>
            </tr>
            <tr>
                <td><code>lightning</code></td>
                <td>Damage caused by being struck by lightning.</td>
            </tr>
            <tr>
                <td><code>magic</code></td>
                <td>Damage caused by being hit by a damage potion or spell.</td>
            </tr>
            <tr>
                <td><code>melting</code></td>
                <td>Damage caused due to a snowman melting.</td>
            </tr>
            <tr>
                <td><code>poison</code></td>
                <td>Damage caused due to an ongoing poison effect.</td>
            </tr>
            <tr>
                <td><code>projectile</code></td>
                <td>Damage caused when attacked by a projectile.</td>
            </tr>
            <tr>
                <td><code>starvation</code></td>
                <td>Damage caused by starving due to having an empty hunger bar.</td>
            </tr>
            <tr>
                <td><code>suffocation</code></td>
                <td>Damage caused by being put in a block.</td>
            </tr>
            <tr>
                <td><code>thorns</code></td>
                <td>Damage caused in retaliation by the thorns enchantment.</td>
            </tr>
            <tr>
                <td><code>void</code></td>
                <td>Damage caused by falling into the void.</td>
            </tr>
            <tr>
                <td><code>wither</code></td>
                <td>Damage caused by Wither potion effect</td>
            </tr>
        </tbody>
    </table>
</div>
