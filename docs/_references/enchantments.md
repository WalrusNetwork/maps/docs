---
layout: references
title: Enchantments
category: gear
order: 1
---

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="25%">Enchantment</th>
                <th width="75%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2"><strong>Armor Enchantments</strong></td>
            </tr>
            <tr>
                <td><code>protection environmental</code></td>
                <td>Provides protection against environmental damage.</td>
            </tr>
            <tr>
                <td><code>protection fire</code></td>
                <td>Provides protection against fire damage.</td>
            </tr>
            <tr>
                <td><code>protection fall</code></td>
                <td>Provides protection against fall damage.</td>
            </tr>
            <tr>
                <td><code>protection explosions</code></td>
                <td>Provides protection against explosive damage.</td>
            </tr>
            <tr>
                <td><code>protection projectile</code></td>
                <td>Provides protection against projectile damage.</td>
            </tr>
            <tr>
                <td><code>oxygen</code></td>
                <td>Decreases the rate of air loss whilst underwater.</td>
            </tr>
            <tr>
                <td><code>water worker</code></td>
                <td>Increases the speed at which a player may mine underwater.</td>
            </tr>
            <tr>
                <td><code>thorns</code></td>
                <td>Damages the attacker.</td>
            </tr>
            <tr>
                <td><code>depth strider</code></td>
                <td>Increases the speed at which a player may move underwater.</td>
            </tr>
            <tr>
                <td colspan="2"><strong>Tool Enchantments</strong></td>
            </tr>
            <tr>
                <td><code>damage all</code></td>
                <td>Increases damage against all targets.</td>
            </tr>
            <tr>
                <td><code>damage arthropods</code></td>
                <td>Increases damage against arthropod targets.</td>
            </tr>
            <tr>
                <td><code>damage undead</code></td>
                <td>Increases damage against undead targets.</td>
            </tr>
            <tr>
                <td><code>knockback</code></td>
                <td>All damage to other targets will knock them back when hit.</td>
            </tr>
            <tr>
                <td><code>fire aspect</code></td>
                <td>When attacking a target, has a chance to set them on fire.</td>
            </tr>
            <tr>
                <td><code>loot bonus mobs</code></td>
                <td>Provides a chance of gaining extra loot when killing monsters.</td>
            </tr>
            <tr>
                <td><code>dig speed</code></td>
                <td>Increases the rate at which you mine/dig.</td>
            </tr>
            <tr>
                <td><code>silk touch</code></td>
                <td>Allows blocks to drop themselves instead of fragments. </td>
            </tr>
            <tr>
                <td><code>durability</code></td>
                <td>Decreases the rate at which a tool looses durability.</td>
            </tr>
            <tr>
                <td><code>loot bonus blocks</code></td>
                <td>Provides a chance of gaining extra loot when destroying blocks.</td>
            </tr>
            <tr>
                <td colspan="2"><strong>Bow Enchantments</strong></td>
            </tr>
            <tr>
                <td><code>arrow damage</code></td>
                <td>Provides extra damage when shooting arrows from bows.</td>
            </tr>
            <tr>
                <td><code>arrow knockback</code></td>
                <td>Provides a knockback when an entity is hit by an arrow from a bow.</td>
            </tr>
            <tr>
                <td><code>arrow fire</code></td>
                <td>Sets entities on fire when hit by arrows shot from a bow.</td>
            </tr>
            <tr>
                <td><code>arrow infinite</code></td>
                <td>Provides infinite arrows when shooting a bow.</td>
            </tr>
            <tr>
                <td colspan="2"><strong>Fishing Rod Enchantments</strong></td>
            </tr>
            <tr>
                <td><code>luck</code></td>
                <td>Decreases odds of catching worthless junk.</td>
            </tr>
            <tr>
                <td><code>lure</code></td>
                <td>Increases rate of fish biting your hook.</td>
            </tr>
        </tbody>
    </table>
</div>
