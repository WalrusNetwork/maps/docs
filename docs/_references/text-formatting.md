---
layout: references
title: Text Formatting
category: misc
order: 2
---

### Usage

Text formatting codes are used to create fancy colorized text, they are prefixed with a caret `^`. After using a formatting code such as `^o` (italic) it has to be reset, otherwise it will continue until the end of the text.

Formatting must follow the `reset` -> `color` -> `function` -> `text` order for everything to properly display. Colors must be specified before functions else the function will not be applied to the text. The reset function at the beginning is optional, but for renamed items or lore without it, the text will be automatically italicized.

```xml

```

### Text Colors

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="3%"></th>
                <th width="5%">Code</th>
                <th width="17%">Color</th>
                <th width="3%"></th>
                <th width="5%">Code</th>
                <th width="17%">Color</th>
                <th width="3%"></th>
                <th width="5%">Code</th>
                <th width="17%">Color</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><div class="color-square" style="background-color:#000000"></div></td>
                <td><code>0</code></td>
                <td>black</td>
                <td><div class="color-square" style="background-color:#0000aa"></div></td>
                <td><code>1</code></td>
                <td>dark blue</td>
                <td><div class="color-square" style="background-color:#00aa00"></div></td>
                <td><code>2</code></td>
                <td>green</td>
            </tr>
            <tr>
                <td><div class="color-square" style="background-color:#00aaaa"></div></td>
                <td><code>3</code></td>
                <td>cyan</td>
                <td><div class="color-square" style="background-color:#aa0000"></div></td>
                <td><code>4</code></td>
                <td>red</td>
                <td><div class="color-square" style="background-color:#aa00aa"></div></td>
                <td><code>5</code></td>
                <td>purple</td>
            </tr>
            <tr>
                <td><div class="color-square" style="background-color:#ffaa00"></div></td>
                <td><code>6</code></td>
                <td>orange</td>
                <td><div class="color-square" style="background-color:#aaaaaa"></div></td>
                <td><code>7</code></td>
                <td>gray</td>
                <td><div class="color-square" style="background-color:#555555"></div></td>
                <td><code>8</code></td>
                <td>dark gray</td>
            </tr>
            <tr>
                <td><div class="color-square" style="background-color:#5555ff"></div></td>
                <td><code>9</code></td>
                <td>blue</td>
                <td><div class="color-square" style="background-color:#55ff55"></div></td>
                <td><code>a</code></td>
                <td>lime</td>
                <td><div class="color-square" style="background-color:#55ffff"></div></td>
                <td><code>b</code></td>
                <td>aqua</td>
            </tr>
            <tr>
                <td><div class="color-square" style="background-color:#ff5555"></div></td>
                <td><code>c</code></td>
                <td>light red</td>
                <td><div class="color-square" style="background-color:#ff55ff"></div></td>
                <td><code>d</code></td>
                <td>pink</td>
                <td><div class="color-square" style="background-color:#ffff55"></div></td>
                <td><code>e</code></td>
                <td>yellow</td>
            </tr>
            <tr>
                <td><div class="color-square" style="background-color:#ffffff"></div></td>
                <td><code>f</code></td>
                <td>white</td>
                <td colspan="6"></td>
            </tr>
        </tbody>
    </table>
</div>

### Text Functions

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="3%"></th>
                <th width="5%">Code</th>
                <th width="17%">Function</th>
                <th width="3%"></th>
                <th width="5%">Code</th>
                <th width="17%">Function</th>
                <th width="3%"></th>
                <th width="5%">Code</th>
                <th width="17%">Function</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td><code>k</code></td>
                <td>obfuscated</td>
                <td></td>
                <td><code>l</code></td>
                <td><strong>bold</strong></td>
                <td></td>
                <td><code>m</code></td>
                <td><strike>strikethrough</strike></td>
            </tr>
            <tr>
                <td></td>
                <td><code>n</code></td>
                <td><u>underline</u></td>
                <td></td>
                <td><code>o</code></td>
                <td><i>italic</i></td>
                <td></td>
                <td><code>r</code></td>
                <td>reset</td>
            </tr>
        </tbody>
    </table>
</div>
