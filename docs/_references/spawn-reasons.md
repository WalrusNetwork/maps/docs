---
layout: references
title: Spawn Reasons
category: entities
order: 1
---

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="25%">Reason</th>
                <th width="75%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>breeding</code></td>
                <td>When an animal breeds to create a child.</td>
            </tr>
            <tr>
                <td><code>build irongolem</code></td>
                <td>When an iron golem is spawned by being built.</td>
            </tr>
            <tr>
                <td><code>build snowman</code></td>
                <td>When a snowman is spawned by being built.</td>
            </tr>
            <tr>
                <td><code>build wither</code></td>
                <td>When a wither boss is spawned by being built.</td>
            </tr>
            <tr>
                <td><code>chunk gen</code></td>
                <td>When a creature spawns due to chunk generation.</td>
            </tr>
            <tr>
                <td><code>cured</code></td>
                <td>When a villager is cured from infection.</td>
            </tr>
            <tr>
                <td><code>custom</code></td>
                <td>When a creature is spawned by plugins.</td>
            </tr>
            <tr>
                <td><code>default</code></td>
                <td>When an entity is missing a SpawnReason.</td>
            </tr>
            <tr>
                <td><code>dispense egg</code></td>
                <td>When a creature is spawned by a dispenser dispensing an egg.</td>
            </tr>
            <tr>
                <td><code>egg</code></td>
                <td>When a creature spawns from an egg.</td>
            </tr>
            <tr>
                <td><code>infection</code></td>
                <td>When a zombie infects a villager.</td>
            </tr>
            <tr>
                <td><code>jockey</code></td>
                <td>When an entity spawns as a jockey of another entity (mostly spider jockeys).</td>
            </tr>
            <tr>
                <td><code>lightning</code></td>
                <td>When a creature spawns because of a lightning strike.</td>
            </tr>
            <tr>
                <td><code>mount</code></td>
                <td>When an entity spawns as a mount of another entity (mostly chicken jockeys).</td>
            </tr>
            <tr>
                <td><code>natural</code></td>
                <td>When something spawns from natural means.</td>
            </tr>
            <tr>
                <td><code>nether portal</code></td>
                <td>When a creature is spawned by nether portal.</td>
            </tr>
            <tr>
                <td><code>ocelot baby</code></td>
                <td>When an ocelot has a baby spawned along with them.</td>
            </tr>
            <tr>
                <td><code>reinforcements</code></td>
                <td>When an entity calls for reinforcements.</td>
            </tr>
            <tr>
                <td><code>silverfish block</code></td>
                <td>When a silverfish spawns from a block.</td>
            </tr>
            <tr>
                <td><code>slime split</code></td>
                <td>When a slime splits.</td>
            </tr>
            <tr>
                <td><code>spawner</code></td>
                <td>When a creature spawns from a spawner.</td>
            </tr>
            <tr>
                <td><code>spawner egg</code></td>
                <td>When a creature spawns from a Spawner Egg.</td>
            </tr>
            <tr>
                <td><code>villager defense</code></td>
                <td>When an iron golem is spawned to defend a village.</td>
            </tr>
            <tr>
                <td><code>villager invasion</code></td>
                <td>When a zombie is spawned to invade a village.</td>
            </tr>
        </tbody>
    </table>
</div>
