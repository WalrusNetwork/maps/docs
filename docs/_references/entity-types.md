---
layout: references
title: Entity Types
category: entities
order: 0
---

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="25%">Entity</th>
                <th width="75%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>armor stand</code></td>
                <td>Armor stand entity used to display weapons/armor.</td>
            </tr>
            <tr>
                <td><code>arrow</code></td>
                <td>An arrow projectile; may get stuck in the ground.</td>
            </tr>
            <tr>
                <td><code>boat</code></td>
                <td>A placed boat.</td>
            </tr>
            <tr>
                <td><code>dropped item</code></td>
                <td>An item resting on the ground.</td>
            </tr>
            <tr>
                <td><code>egg</code></td>
                <td>A flying chicken egg.</td>
            </tr>
            <tr>
                <td><code>ender crystal</code></td>
                <td>An ender crystal.</td>
            </tr>
            <tr>
                <td><code>ender pearl</code></td>
                <td>A flying ender pearl.</td>
            </tr>
            <tr>
                <td><code>ender signal</code></td>
                <td>An flying ender eye.</td>
            </tr>
            <tr>
                <td><code>experience orb</code></td>
                <td>An experience orb.</td>
            </tr>
            <tr>
                <td><code>falling block</code></td>
                <td>A block that is going to or is about to fall.</td>
            </tr>
            <tr>
                <td><code>fireball</code></td>
                <td>A flying large fireball, as thrown by a Ghast for example.</td>
            </tr>
            <tr>
                <td><code>firework</code></td>
                <td>A flying firework.</td>
            </tr>
            <tr>
                <td><code>fishing hook</code></td>
                <td>A fishing line and bobber.</td>
            </tr>
            <tr>
                <td><code>item frame</code></td>
                <td>An item frame on a wall.</td>
            </tr>
            <tr>
                <td><code>leash hitch</code></td>
                <td>A leash attached to a fencepost.</td>
            </tr>
            <tr>
                <td><code>lightning</code></td>
                <td>A bolt of lightning.</td>
            </tr>
            <tr>
                <td><code>minecart</code></td>
                <td>A minecart.</td>
            </tr>
            <tr>
                <td><code>minecart chest</code></td>
                <td>A minecart with a chest in it.</td>
            </tr>
            <tr>
                <td><code>minecart command</code></td>
                <td>A minecart with a command block in it.</td>
            </tr>
            <tr>
                <td><code>minecart furnace</code></td>
                <td>A powered minecart.</td>
            </tr>
            <tr>
                <td><code>minecart hopper</code></td>
                <td>A minecart with a hopper in it.</td>
            </tr>
            <tr>
                <td><code>minecart mob spawner</code></td>
                <td>A minecart with a mob spawner in it.</td>
            </tr>
            <tr>
                <td><code>minecart tnt</code></td>
                <td>A minecart with TNT in it.</td>
            </tr>
            <tr>
                <td><code>painting</code></td>
                <td>A painting on a wall.</td>
            </tr>
            <tr>
                <td><code>primed tnt</code></td>
                <td>Primed TNT that is about to explode.</td>
            </tr>
            <tr>
                <td><code>snowball</code></td>
                <td>A flying snowball.</td>
            </tr>
            <tr>
                <td><code>small fireball</code></td>
                <td>A flying small fireball, such as thrown by a Blaze or player.</td>
            </tr>
            <tr>
                <td><code>splash potion</code></td>
                <td>A flying splash potion.</td>
            </tr>
            <tr>
                <td><code>thrown exp bottle</code></td>
                <td>A flying experience bottle.</td>
            </tr>
            <tr>
                <td><code>wither skull</code></td>
                <td>A flying wither skull projectile.</td>
            </tr>
            <tr>
                <td colspan="2"><strong>Creature Types</strong></td>
            </tr>
            <tr>
                <td><code>bat</code></td>
                <td>A bat.</td>
            </tr>
            <tr>
                <td><code>blaze</code></td>
                <td>A blaze.</td>
            </tr>
            <tr>
                <td><code>cave spider</code></td>
                <td>A cave spider.</td>
            </tr>
            <tr>
                <td><code>chicken</code></td>
                <td>A chicken.</td>
            </tr>
            <tr>
                <td><code>cow</code></td>
                <td>A cow.</td>
            </tr>
            <tr>
                <td><code>creeper</code></td>
                <td>A creeper.</td>
            </tr>
            <tr>
                <td><code>ender dragon</code></td>
                <td>An ender dragon.</td>
            </tr>
            <tr>
                <td><code>enderman</code></td>
                <td>An enderman.</td>
            </tr>
            <tr>
                <td><code>endermite</code></td>
                <td>An endermite.</td>
            </tr>
            <tr>
                <td><code>ghast</code></td>
                <td>A ghast.</td>
            </tr>
            <tr>
                <td><code>giant</code></td>
                <td>A giant.</td>
            </tr>
            <tr>
                <td><code>guardian</code></td>
                <td>A guardian or elder guardian.</td>
            </tr>
            <tr>
                <td><code>horse</code></td>
                <td>A horse.</td>
            </tr>
            <tr>
                <td><code>iron golem</code></td>
                <td>An iron golem.</td>
            </tr>
            <tr>
                <td><code>magma cube</code></td>
                <td>A magma cube.</td>
            </tr>
            <tr>
                <td><code>mushroom cow</code></td>
                <td>A mooshroom.</td>
            </tr>
            <tr>
                <td><code>ocelot</code></td>
                <td>An ocelot.</td>
            </tr>
            <tr>
                <td><code>pig</code></td>
                <td>A pig.</td>
            </tr>
            <tr>
                <td><code>pig zombie</code></td>
                <td>A zombie pigman.</td>
            </tr>
            <tr>
                <td><code>rabbit</code></td>
                <td>A rabbit or killer rabbit.</td>
            </tr>
            <tr>
                <td><code>silverfish</code></td>
                <td>A silverfish.</td>
            </tr>
            <tr>
                <td><code>skeleton</code></td>
                <td>A skeleton or wither skeleton.</td>
            </tr>
            <tr>
                <td><code>sheep</code></td>
                <td>A sheep.</td>
            </tr>
            <tr>
                <td><code>slime</code></td>
                <td>A slime.</td>
            </tr>
            <tr>
                <td><code>snowman</code></td>
                <td>A snowman.</td>
            </tr>
            <tr>
                <td><code>spider</code></td>
                <td>A spider.</td>
            </tr>
            <tr>
                <td><code>squid</code></td>
                <td>A squid.</td>
            </tr>
            <tr>
                <td><code>villager</code></td>
                <td>A villager.</td>
            </tr>
            <tr>
                <td><code>witch</code></td>
                <td>A witch.</td>
            </tr>
            <tr>
                <td><code>wither</code></td>
                <td>A wither.</td>
            </tr>
            <tr>
                <td><code>wolf</code></td>
                <td>A wolf.</td>
            </tr>
            <tr>
                <td><code>zombie</code></td>
                <td>A zombie or zombie villager.</td>
            </tr>
        </tbody>
    </table>
</div>
