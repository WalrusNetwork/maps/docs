---
layout: references
title: Effects
category: gear
order: 3
---

Each effect represents of type of potion effect that can be applied to a player or potion item.

An _effect_ must only be referenced when using the `<effect>...</effect>` kit sub-element. A _potion_, on the other hand, must only be referenced when applying a `potion="..."` to a potion item. A potion type _cannot_ be referenced in the effect kit sub-element, and vice versa.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="25%">Effect Type</th>
                <th width="25%">Potion Type</th>
                <th width="50%">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>absorption</code></td>
                <td></td>
                <td>Increases the maximum health of an entity with health that cannot be regenerated.</td>
            </tr>
            <tr>
                <td><code>blindness</code></td>
                <td></td>
                <td>Blinds an entity.</td>
            </tr>
            <tr>
                <td><code>confusion</code></td>
                <td></td>
                <td>Warps vision on the client.</td>
            </tr>
            <tr>
                <td><code>damage resistance</code></td>
                <td></td>
                <td>Decreases damage dealt to an entity.</td>
            </tr>
            <tr>
                <td><code>fast digging</code></td>
                <td></td>
                <td>Increases dig speed.</td>
            </tr>
            <tr>
                <td><code>fire resistance</code></td>
                <td><code>fire resistance</code></td>
                <td>Stops fire damage.</td>
            </tr>
            <tr>
                <td><code>harm</code></td>
                <td><code>instant damage</code></td>
                <td>Hurts an entity.</td>
            </tr>
            <tr>
                <td><code>heal</code></td>
                <td><code>instant heal</code></td>
                <td>Heals an entity.</td>
            </tr>
            <tr>
                <td><code>health boost</code></td>
                <td></td>
                <td>Increases the maximum health of an entity.</td>
            </tr>
            <tr>
                <td><code>hunger</code></td>
                <td></td>
                <td>Increases hunger.</td>
            </tr>
            <tr>
                <td><code>increase damage</code></td>
                <td><code>strength</code></td>
                <td>Increases damage dealt.</td>
            </tr>
            <tr>
                <td><code>invisibility</code></td>
                <td><code>invisibility</code></td>
                <td>Grants invisibility.</td>
            </tr>
            <tr>
                <td><code>jump</code></td>
                <td><code>jump</code></td>
                <td>Increases jump height.</td>
            </tr>
            <tr>
                <td><code>night vision</code></td>
                <td><code>night vision</code></td>
                <td>Allows an entity to see in the dark.</td>
            </tr>
            <tr>
                <td><code>poison</code></td>
                <td><code>poison</code></td>
                <td>Deals damage to an entity over time.</td>
            </tr>
            <tr>
                <td><code>regeneration</code></td>
                <td><code>regen</code></td>
                <td>Regenerates health.</td>
            </tr>
            <tr>
                <td><code>saturation</code></td>
                <td></td>
                <td>Increases the food level of an entity each tick.</td>
            </tr>
            <tr>
                <td><code>slow</code></td>
                <td><code>slowness</code></td>
                <td>Decreases movement speed.</td>
            </tr>
            <tr>
                <td><code>slow digging</code></td>
                <td></td>
                <td>Decreases dig speed.</td>
            </tr>
            <tr>
                <td><code>speed</code></td>
                <td><code>speed</code></td>
                <td>Increases movement speed.</td>
            </tr>
            <tr>
                <td></td>
                <td><code>water</code></td>
                <td>An empty water bottle potion.</td>
            </tr>
            <tr>
                <td><code>water breathing</code></td>
                <td><code>water breathing</code></td>
                <td>Allows breathing underwater.</td>
            </tr>
            <tr>
                <td><code>weakness</code></td>
                <td><code>weakness</code></td>
                <td>Decreases damage dealt by an entity.</td>
            </tr>
            <tr>
                <td><code>wither</code></td>
                <td></td>
                <td>Deals damage to an entity over time.</td>
            </tr>
        </tbody>
    </table>
</div>
