---
layout: guides
title: Order of Applicators
category: basics
order: 3
---

### Introduction

The order of applicators and how to properly define them has always been a tricky concept to grasp. Here we will walk through how an XML definition translates into real examples to better understand how you can apply filters to regions.

In this guide we will cover:

- How applicators define precedence
- A real example and scenarios

### Understanding Precedence

The order in which <code>{{'<apply>' | escape}}</code> elements are specified determines which takes precedence when multiple regions overlap in applicators. Applicators specified first override those specified further down in the XML.

1. When a specific event occurs, the event context (players involved, entities, materials, etc) are all checked against the defined applicators starting at the top of the `<applicators>` list.
2. The first condition that is checked is that the event context occurred within the region that the applicator is for. If the event did not occur within the region, the applicator is ignored. Applicators without a region are applied to `everywhere`.
2. When an applicator with the correct context is found, the assigned filters are checked in context with the event type (block place event, use event, region enter event, etc.). For example, a block place event will only respond to applicators with `block="..."` and `block-place="..."` attributes. If no appropriate event types have been defined, that applicator will `abstain` from returning either `allow` or `deny` and the next applicator will be checked.
3. Once an applicator has been matched to the event, the filter will be tested against the context. The filter can return `allow` or `deny` which will allow the event to happen or stop it. Once an applicator has returned `allow` or `deny` for an event, no other applicators will be checked. However, if the filter returns `abstain`, the applicator will do nothing and the next applicator in the list will be checked.

### Example

```xml
<filters>
    <!-- deny any material, allow everything else -->
    <not id="banned-blocks">
        <any>
            <material>tnt</material>
            <material>dispenser</material>
            <material>redstone wire</material>
        </any>
    </not>
    <!-- deny wool, otherwise abstain -->
    <deny id="deny-safety-wool">
        <material>wool</material>
    </deny>
    <!-- deny everything that is considered void, allow everything else -->
    <not id="not-void">
        <void/>
    </not>
</filters>
<regions>
    <cylinder id="region-1" base="0.5,0,0.5" radius="12" height="10"/>
    <cuboid id="region-2" min="0,0,0" max="16,10,16"/>
    <cuboid id="region-3" min="13,0,13" max="16,10,16"/>
</regions>
<applicators>
    <apply block-place="deny-safety-wool" message="{region.error.place.safety-wool}"/>
    <apply region="region-3" block="always"/>
    <apply region="region-1" block="never" message="{region.error.modify}"/>
    <apply region="region-2" block="banned-blocks" message="{region.error.modify}"/>
    <apply block="not-void" message="{region.error.modify.void}"/>
</applicators>
```

<div class="row">
    <div class="col-lg-8">
        <p>Consider the example filters, regions (visual aid to the right), and applicators above. As you can see, <code>region-1</code> is partially overlapped by <code>region-2</code>, which also completely overlaps with <code>region-3</code>. Imagine that everything outside of these regions is void. We have also defined three different filters:</p>
        <ol>
            <li><code>deny-safety-wool</code>, which returns DENY in contexts where wool is concerned. All other contexts will ABSTAIN.</li>
            <li><code>banned-blocks</code>, which returns DENY in contexts where tnt, dispensers, <i>or</i> redstone wire is concerned. All other contexts will return ALLOW.</li>
            <li><code>not-void</code>, which returns DENY in contexts where the void is concerned. All other contexts will retuen ALLOW.</li>
        </ol>
        <p>Additionally, we have the following applicators defined:</p>
        <ol>
            <li>In <code>everywhere</code> we are applying the <code>deny-safety-wool</code> filter for <code>block-place</code> event contexts.</li>
            <li>In <code>region-3</code> we are applying the <code>always</code> filter for <code>block</code> event contexts.</li>
            <li>In <code>region-1</code> we are applying the <code>never</code> filter for <code>block</code> event contexts.</li>
            <li>In <code>region-2</code> we are applying the <code>banned-blocks</code> filter for <code>block</code> event contexts.</li>
            <li>In <code>everywhere</code> we are applying the <code>not-void</code> filter for <code>block</code> event contexts.</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <img src="{{ site.baseurl }}/assets/img/regions/applicators-example.png" width="75%" class="mx-auto d-block">
    </div>
</div>

### Scenario A

```xml
<filters>
    <!-- deny any material, allow everything else -->
    <not id="banned-blocks">
        <any>
            <material>tnt</material>
            <material>dispenser</material>
            <material>redstone wire</material>
        </any>
    </not>
    <!-- deny wool, otherwise abstain -->
    <deny id="deny-safety-wool">
        <material>wool</material>
    </deny>
    <!-- deny everything that is considered void, allow everything else -->
    <not id="not-void">
        <void/>
    </not>
</filters>
<regions>
    <cylinder id="region-1" base="0.5,0,0.5" radius="12" height="10"/>
    <cuboid id="region-2" min="0,0,0" max="16,10,16"/>
    <cuboid id="region-3" min="13,0,13" max="16,10,16"/>
</regions>
<applicators>
    <apply block-place="deny-safety-wool" message="{region.error.place.safety-wool}"/>
    <apply region="region-3" block="always"/>
    <apply region="region-1" block="never" message="{region.error.modify}"/>
    <apply region="region-2" block="banned-blocks" message="{region.error.modify}"/>
    <apply block="not-void" message="{region.error.modify.void}"/>
</applicators>
```

<img src="{{ site.baseurl }}/assets/img/regions/applicators-example-scenario-a.png" class="mx-auto d-block">

[quiz question="Given the above XML sample, what would happen if a player on Red team placed a wood block in the top-left corner of 'region-2'?" responses="The block place event is allowed.;The block place event is denied." correct="0" reason="Explanation in the next section."]

### Explaination A

The block place event would be allowed.

```xml
<filters>
    <!-- deny any material, allow everything else -->
    <not id="banned-blocks">
        <any>
            <material>tnt</material>
            <material>dispenser</material>
            <material>redstone wire</material>
        </any>
    </not>
    <!-- deny wool, otherwise abstain -->
    <deny id="deny-safety-wool">
        <material>wool</material>
    </deny>
    <!-- deny everything that is considered void, allow everything else -->
    <not id="not-void">
        <void/>
    </not>
</filters>
<regions>
    <cylinder id="region-1" base="0.5,0,0.5" radius="12" height="10"/>
    <cuboid id="region-2" min="0,0,0" max="16,10,16"/>
    <cuboid id="region-3" min="13,0,13" max="16,10,16"/>
</regions>
<applicators>
    <apply block-place="deny-safety-wool" message="{region.error.place.safety-wool}"/>
    <apply region="region-3" block="always"/>
    <apply region="region-1" block="never" message="{region.error.modify}"/>
    <apply region="region-2" block="banned-blocks" message="{region.error.modify}"/>
    <apply block="not-void" message="{region.error.modify.void}"/>
</applicators>
```

When A player on Red team places a wood block in the top-left corner of `region-2`, the following process is taken to check if the event should be permitted:

- The 1st applicator is checked since the event is occurring inside of `everywhere`.
    - The event is a block-place based event, so the `deny-safety-wool` filter is checked.
    - `wood` is not included in the filter, so the filter returns `abstain` and the next applicator is checked.
- The 2nd applicator abstains since the event is occurring outside of `region-3`.
- The 3rd applicator abstains since the event is occurring outside of `region-1`.
- The 4th applicator is checked since the event is occurring inside of `region-2`.
    - The event is a block based event, so the `banned-blocks` filter is checked.
    - 1wood1 is not included in the filter, so the filter returns `allow`.
- The block place event is **allowed** and no further applicators are checked.

### Scenario B

```xml
<filters>
    <!-- deny any material, allow everything else -->
    <not id="banned-blocks">
        <any>
            <material>tnt</material>
            <material>dispenser</material>
            <material>redstone wire</material>
        </any>
    </not>
    <!-- deny wool, otherwise abstain -->
    <deny id="deny-safety-wool">
        <material>wool</material>
    </deny>
    <!-- deny everything that is considered void, allow everything else -->
    <not id="not-void">
        <void/>
    </not>
</filters>
<regions>
    <cylinder id="region-1" base="0.5,0,0.5" radius="12" height="10"/>
    <cuboid id="region-2" min="0,0,0" max="16,10,16"/>
    <cuboid id="region-3" min="13,0,13" max="16,10,16"/>
</regions>
<applicators>
    <apply block-place="deny-safety-wool" message="{region.error.place.safety-wool}"/>
    <apply region="region-3" block="always"/>
    <apply region="region-1" block="never" message="{region.error.modify}"/>
    <apply region="region-2" block="banned-blocks" message="{region.error.modify}"/>
    <apply block="not-void" message="{region.error.modify.void}"/>
</applicators>
```

<img src="{{ site.baseurl }}/assets/img/regions/applicators-example-scenario-b.png" class="mx-auto d-block">

[quiz question="Given the above XML sample, what would happen if a player on Red team placed a TNT block in the top-right corner of 'region-1'?" responses="The block place event is allowed.;The block place event is denied." correct="0" reason="Explanation in the next section."]

### Explaination B

The block place event would be allowed.

```xml
<filters>
    <!-- deny any material, allow everything else -->
    <not id="banned-blocks">
        <any>
            <material>tnt</material>
            <material>dispenser</material>
            <material>redstone wire</material>
        </any>
    </not>
    <!-- deny wool, otherwise abstain -->
    <deny id="deny-safety-wool">
        <material>wool</material>
    </deny>
    <!-- deny everything that is considered void, allow everything else -->
    <not id="not-void">
        <void/>
    </not>
</filters>
<regions>
    <cylinder id="region-1" base="0.5,0,0.5" radius="12" height="10"/>
    <cuboid id="region-2" min="0,0,0" max="16,10,16"/>
    <cuboid id="region-3" min="13,0,13" max="16,10,16"/>
</regions>
<applicators>
    <apply block-place="deny-safety-wool" message="{region.error.place.safety-wool}"/>
    <apply region="region-3" block="always"/>
    <apply region="region-1" block="never" message="{region.error.modify}"/>
    <apply region="region-2" block="banned-blocks" message="{region.error.modify}"/>
    <apply block="not-void" message="{region.error.modify.void}"/>
</applicators>
```

- The 1st applicator is checked since the event is occurring inside of `everywhere`.
    - The event is a block-place based event, so the `deny-safety-wool` filter is checked.
    - `tnt` is not included in the filter, so the filter returns `abstain` and the next applicator is checked.
- The 2nd applicator is checked since the event is occurring inside of `region-1`.
    - The event is a block-place based event, so the `always` filter is checked.
    - `always` will always return `allow`.
- The block place event is **allowed** and no further applicators are checked.

_Despite TNT being placed inside of `region-2` where TNT is not allowed to be placed, `region-3` always allows blocks to be placed. Since the applicator for `region-3` comes first, the result of this rule takes precedence._

### Final Notes

If at any point you feel as though you need some extra assistance with understanding something or realizing a concept, feel free to chat with us in our [Discord](https://walrus.gg/discord).
