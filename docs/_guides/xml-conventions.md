---
layout: guides
title: XML Conventions
category: basics
order: 1
---

### Introduction

Instead of having each XML being written in their own style, these XML conventions tell you how you must write your XMLs. This ensures that majority of the XML documents are written in a consistent style to increase understandability and allows other people looking at the XML to know what to expect throughout the document.

In this guide we will cover:

- The XML conventions used by Walrus

### Indentation

#### Map Indentation

All facets must start on column 1 of the XML document. This means that all children tags of <code>{{'<map>' | escape}}</code> must be aligned with the element.

```xml
<?xml version="1.0"?>
<map proto="2.0.0">
<name>Dry Outpost</name>
<version>1.0.1</version>
<game>ctw</game>
<objective>Capture the wool and return it to your victory monument!</objective>
</map>
```

#### Child Element Indentation

Facets that have sub-elements, such as <code>{{'<filters>' | escape}}</code> must be indented with 4 spaces below the parent element. This also applies for all subsequent child elments.

```xml
<?xml version="1.0"?>
<map proto="2.0.0">
<filters>
    <team id="only-blue">blue</team>
    <team id="only-red">red</team>
    <not id="not-void">
        <void/>
    </not>
</filters>
</map>
```

#### Empty Lines

There must be no empty lines between facets. The end of one facet must be the beginning of the next facet on the immediate next line.

#### File Endings

XML files must end with an empty line. For example, if the closing <code>{{'</map>' | escape}}</code> is on line 52, then the last line of the XML file will be line 53 containing nothing.

#### Spacing

Spacing between element attributes may be used to neatly align the attributes to increase readability.

```xml
<hills capture-time="6s" points="1">
    <hill name="Point A" capture-region="a-capture"      progress-region="a-progress"      control-region="a-control"/>
    <hill name="Middle"  capture-region="middle-capture" progress-region="middle-progress" control-region="middle-capture"/>
    <hill name="Point B" capture-region="b-capture"      progress-region="b-progress"      control-region="b-control"/>
</hills>
```

[quiz question="XML documents must be indented with:" responses="4 spaces;4 tabs;2 spaces;2 tabs" correct="0" reason="Walrus uses 4 spaces for all XML documents."]

### Structure

#### Main Map Elements

The main map elements are composed of 7 different facets which are to be placed at the top of the XML document in the following order inside of the main <code>{{'<map>' | escape}}</code> element: <code>{{'<name>' | escape}}</code>, <code>{{'<version>' | escape}}</code>, <code>{{'<game>' | escape}}</code>, <code>{{'<objective>' | escape}}</code>, <code>{{'<authors>' | escape}}</code>, and <code>{{'<contributors>' | escape}}</code> (if applicable).

```xml
<?xml version="1.0"?>
<map proto="2.0.0">
<name>Dry Outpost</name>
<version>1.0.1</version>
<game>ctw</game>
<objective>Capture the wool and return it to your victory monument</objective>
<authors>
    <author uuid="f690a591-348b-482e-a18d-7779d0c0a28c"/> <!-- mitchiii_ -->
</authors>
</map>
```

#### Protocol

All new must must use the newest protocol. The current protocol is {{site.current_proto}}.

#### Authors and Contributors

When defining authors and contributors, include their current account name in a comment next to the XML element. This is so that the author can be easily identified when looking at the XML document without having to look up the UUID.

```xml
<authors>
    <author uuid="f690a591-348b-482e-a18d-7779d0c0a28c"/> <!-- mitchiii_ -->
</authors>
```

#### Other Facets

The remaining facets must be placed in the XML document in an order similar to the following: <code>{{'<teams>' | escape}}</code>, <code>{{'<kits>' | escape}}</code>, <code>{{'<spawns>' | escape}}</code>, <code>{{'<filters>' | escape}}</code>, <code>{{'<regions>' | escape}}</code>, <code>{{'<applicators>' | escape}}</code>, game objectives, <code>{{'<kill-rewards>' | escape}}</code>, <code>{{'<items>' | escape}}</code>, and then anything else required by the map. Similar facets must be grouped together.

### Naming

#### Map Name

Map names must not contain any special characters or symbols. This includes both unicodes and letters with accents.

#### Versioning

Map versions must follow the Semantic Versioning Schema -- <code>major.minor.patch</code>.

A Major is classified by the addition of major gameplay changes - changes that could be called a new map. A Minor is classified by the addition of changes that affect gameplay but not to the extent of a major. A Patch is classified by a small change to fix a bug or unintentional feature.

#### Identification Naming

Unique IDs must use the <a href="https://en.wikipedia.org/wiki/Kebab_case" target="_blank">dash-case</a> naming scheme.

```xml
<cuboid id="red-base" min="113,43,31" max="119,51,11"/>
<cuboid id="blue-base" min="113,43,155" max="119,51,175"/>
```

#### Enumeration Naming

Names which are defined as enumeration in bukkit must be transferred into lower case and _ underscores replaced with spaces. <code>PROTECTION_ENVIRONMENTAL</code> would become <code>protection environmental</code>.

#### Team Filters

Team filters must be named by following _Identification Naming_ as well as starting with <code>only-</code> followed by the name of the team.

```xml
<team id="only-blue">blue</team>
```

[quiz question="Which of the following is a valid map version?" responses="0.1;1.0.12;1-beta;1.0" correct="1" reason="Map versions must follow the major.minor.patch versioning schema and may only contain numbers."]

### Translations

Strings in the XML that are displayed to the player, where appropriate, must use a respective [translation string]({{ site.baseurl }}/references/translation-strings). This includes the map `<objective>` and `<applicators>` error messages.

```xml
<applicators>
    <apply region="spawns" block="never" message="{region.error.modify.spawn}"/>
    <apply region="blue-wools" block="only-blue" message="{region.error.modify.own-wool}"/>
    <apply region="red-wools" enter="only-red" message="{region.error.enter.own-wool}"/>
</applicators>
```

[quiz question="Which of the following translation keys would be most appropriate for when one team may not enter the other team's spawn?" responses="{region.error.enter.enemy-territory};{region.error.modify.spawn};{region.error.enter};{region.error.enter.enemy-spawn}" correct="3" reason="{region.error.enter-enemy-spawn} is the only translation key that mentions that you cannot enter the enemy team's spawn."]

### Final Notes

If at any point you feel as though you need some extra assistance with understanding something or realizing a concept, feel free to chat with us in our [Discord](https://walrus.gg/discord).
