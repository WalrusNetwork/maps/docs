---
layout: guides
title: Getting Started
category: basics
order: 0
---

### Introduction

The XML configuration file is the backbone of each map, allowing Walrus' custom plugins to interpret and load custom match facets in order to run the map. From the map's spawns, to kits, and even the objective, almost everything can be configured through the map's XML file. This documentation is intended to list all currently available XML facets, their attributes and sub-elements, and describe how the modules work and interact with the player.

In this guide we will cover:

- Recommended tools and resources
- How to read the documentation
- How you can customize the documentation site
- Getting started with your first XML

### Tools

Before you begin creating your first XML, you need to have the right tools for the job. Any coding text editor would be sufficient, and paid software is no where near a requirement. However, the following text editors are tried and recommended by the Walrus team:

- [Atom](https://atom.io/)
- [Brackets](http://brackets.io/)
- [Notepad++](https://notepad-plus-plus.org/)

Each of these are free, open source, and have a range of community made plugins to tailor your experience with them. Use the one you feel most comfortable with, or, if you have something else you like more, use that instead.

[quiz question="What program is recommended for creating XML documents?" responses="Paid text editors.;An IDE (integrated development environment).;Any text editor designed for coding.;None. You just need a pen and paper." correct="2" reason="Any text editor designed for coding is more than sufficient."]

### Reading the Docs

The documentation site is divided up into the following sections:

- [Facets]({{ site.baseurl }}/facets/required-elements), which covers the main facet documentation and examples
- [References]({{ site.baseurl }}/references/inventory-materials), which defines attribute data types and enumerations
- [Guides]({{ site.baseurl}}/guides/introduction), which leads you through the XML process
- [Examples]({{ site.baseurl }}/examples/templates), which demonstrates advanced examples and extended templates

Majority of the facets are documented in tables similar to the following. Some features of this facet have been omitted for the sake of this example.

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Element</th>
                <th width="55%">Description</th>
                <th width="25%">Children</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>{{'<wools>' | escape}}</code></td>
                <td>Element containing all wool objectives.</td>
                <td><span class="badge badge-secondary">Wools Sub-elements</span></td>
            </tr>
            <tr>
                <td colspan="3"><strong>Sub-elements</strong></td>
            </tr>
            <tr>
                <td><code>{{'<wool>' | escape}}</code></td>
                <td>A single wool objective.</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="h4"><code>{{'<wool>' | escape}}</code> Attributes</div>

<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th width="20%">Attribute</th>
                <th width="55%">Description</th>
                <th width="15%">Value</th>
                <th width="10%">Default</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>id</code></td>
                <td>Unique identifier used to reference cores from other places in the XML.</td>
                <td><span class="badge badge-primary">Unique ID</span></td>
                <td></td>
            </tr>
            <tr>
                <td><code>destination</code></td>
                <td><span class="badge badge-danger">Required</span> The monument where the wool has to be placed.</td>
                <td><a href="#">Bounded Region</a></td>
                <td></td>
            </tr>
            <tr>
                <td><code>craftable</code></td>
                <td>Specify if this wool can be crafted with wool and dye.</td>
                <td><span class="badge badge-primary">Boolean</span></td>
                <td>true</td>
            </tr>
        </tbody>
    </table>
</div>

The <code>Element</code>, <code>Sub-element</code>, and <code>Attribute</code> table columns tell you _what_ the thing is, while the <code>Description</code> column tells you its purpose. Sometimes in the description, things will have the <span class="badge badge-danger">Required</span> badge. This means that this thing is required in order for the XML to load with that facet.

The <code>Value</code>, <code>Children</code>, or <code>Element Text</code> column inform you what the element accepts as a child or what data types attributes need. The <span class="badge badge-secondary">Light Blue Badge</span> represents a set of facet specific sub-elements, the <span class="badge badge-primary">Dark Blue Badge</span> represents a [specific attribute data type]({{ site.baseurl }}/references/attribute-types), and a [Link](#) represents a reference to the ID or a set of sub-elements of the specified type, depending on the context. Some attributes have default values if nothing is specified, and these are shown in the <code>Default</code> column of the table.

[quiz question="In the above example facet documentation, which attribute must be explicitly defined in the XML?" responses="id;wool;craftable;destination" correct="3" reason="Destination is the only attribute that has the 'Required' badge, and thus is the only attribute that must be explicitly defined."]

### Personalization

The documentation site can be personalized to your liking with the following features.

#### Site Preferences

The documentation site offers various preference settings which you can customize in order to best suit the site to your working experience. You can access your preferences <strong><a href="#" data-toggle="modal" data-target="#userPreferencesModal">here</a></strong> or by clicking the `Preferences` link in the site navigation at any time. These preferences are stored in your browser as cookies, so they are only accessible by you.

#### Bookmarking Sections

If you find yourself going back to certain pages or sections often for reference, you can bookmark them for quick access later by clicking on the <i class="far fa-bookmark"></i> bookmark icon next to any heading when you hover over it. You can then access these bookmarks in your bookmarks menu <strong><a href="#" data-toggle="modal" data-target="#userBookmarksModal">here</a></strong> or by clicking the `Bookmarks` link in the site navigation at any time. You can remove bookmarks by clicking the <i class="fas fa-times"></i> cross in the bookmarks menu, or by clicking the <i class="fas fa-bookmark"></i> bookmark icon again.

### Quick Start

You can find a bare-bones template covering all of the main Required Elements [here]({{ site.baseurl }}/facets/required-elements#basic-template). Instead, if you would prefer more complete templates, can find templates for each gamemode [here]({{ site.baseurl }}/examples/templates).

### Final Notes

If at any point you feel as though you need some extra assistance with understanding something or realizing a concept, feel free to chat with us in our [Discord](https://walrus.gg/discord).
