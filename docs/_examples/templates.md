---
layout: examples
title: Overview
category: templates
order: 0
---

Templates are a great way to learn and understand how each of the individual facets are put together to great a functioning map configuration. Below are a set of generic XML templates for each of the core game types.

Each XML has all the facets required by the game already included, with the only work you have to do is filling out coordinates. These templates will not suit every map, however. You may need to add additional facets or change some regions and filters. Because of this, it is important that you still have an understanding of what it is the XML is doing and how to correctly implement the features you need.

<div class="row">
    {% assign examples = site.examples | where:"category","templates" | sort:"order" %}
    {% for example in examples %}
    {% unless example.title == "Overview" %}
    <div class="col-sm-12 col-md-4 mb-4">
        <a href="{{ site.baseurl }}{{example.url}}" class="text-decoration-none text-reset">
            <div class="card grow">
                <div class="card-body">
                    <h5 style="font-weight: bold">{{example.title}}</h5>
                    <p class="card-text">{{example.description}}</p>
                </div>
            </div>
        </a>
    </div>
    {% endunless %}
    {% endfor %}
</div>
