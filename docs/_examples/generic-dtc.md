---
layout: examples
title: Generic DTC
category: templates
order: 3
description: A 2-team Destroy the Core map where each team must leak lava from the enemy's core in order to win the match.
---

The following template is for a 2-team Destroy the Core map where each team must leak lava from the enemy's core in order to win the match.

<div class="callout callout-warning">Remember to remove the inline comments as you fill out the XML!</div>

<!-- todo <script src="https://gitlab.com/WalrusNetwork/maps/docs/snippets/1868578.js"></script> -->
