---
layout: examples
title: Generic CTW
category: templates
order: 1
description: A 2-team Capture the Wool map where each team must capture a single wool each in order to win the match.
---

The following template is for a 2-team Capture the Wool map where each team must capture a single wool each in order to win the match.

<div class="callout callout-warning">Remember to remove the inline comments as you fill out the XML!</div>

<script src="https://gitlab.com/WalrusNetwork/maps/docs/snippets/1923862.js"></script>
