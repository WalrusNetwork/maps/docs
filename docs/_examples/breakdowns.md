---
layout: examples
title: Overview
category: breakdowns
order: 0
---
The following sections consist of XMLs from specific maps that are played on Walrus. Each page goes in depth on each part of the Map's XML and why it is used.